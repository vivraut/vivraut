package com.vivraut.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vivraut.entity.*;
import com.vivraut.dao.*;

@RestController
public class TutorRestController {

	@Autowired
	TutorRepository dao;
	
    @RequestMapping(value="/tutors", method = RequestMethod.GET,headers="Accept=application/json")
    public List<Tutor> getTutors(@RequestParam(value="name", defaultValue="World") String name) {
        List<Tutor> results = new ArrayList<>();
        results = (List<Tutor>) dao.findAll();
        //dao.findAll().forEach(element -> results.add(element));
        /*Iterable<Activity> iter = dao.findAll();
        for (Activity activity : iter) {
        	results.add(String.valueOf(activity.getIdActivity()) + " - " + activity.getNameActivity() + " (" + activity.getAutism().getFirstnameAutism() + ")");
        	//results.add(activity);
		}*/
        return results;
    }
}
