package com.vivraut.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.vivraut.entity.*;
import com.vivraut.dao.*;
import com.vivraut.dto.AutismDto;

@RestController
public class AutismRestController extends AbstractRestController {

	@Autowired
	AutismRepository autRepo;

	private ModelMapper modelMapper = new ModelMapper();

	/**
	 * Retourne la liste des autistes que peut gérer le tuteur.
	 */
	@RequestMapping(value = "/autistic", method = RequestMethod.GET, headers = "Accept=application/json")
	protected List<AutismDto> read(HttpSession session) {
		List<Autism> autistics = null;
		if (isTutorConnected(session)) {
			Tutor tutor = getTutorFromSession(session);
			autistics = autRepo.findByTutor(tutor);
		}

		List<AutismDto> autDto = new ArrayList<>();
		if (autistics != null) {
			autistics.forEach(a -> autDto.add(modelMapper.map(a, AutismDto.class)));
		}
		return autDto;
	}

	/**
	 * Met à jour un autiste.
	 */
	@RequestMapping(value = "/autistic", method = RequestMethod.POST, headers = "Accept=application/json")
	protected void update(HttpSession session) {
		System.out.println("update");
	}

	/**
	 * Supprime un autiste.
	 */
	@RequestMapping(value = "/autistic", method = RequestMethod.DELETE, headers = "Accept=application/json")
	protected void delete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("delete");
	}

	/**
	 * Ajoute un autiste.
	 */
	@RequestMapping(value = "/autistic", method = RequestMethod.PUT, headers = "Accept=application/json")
	protected void create(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("create");
	}

}
