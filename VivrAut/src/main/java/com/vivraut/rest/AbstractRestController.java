package com.vivraut.rest;

import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpSession;

import com.vivraut.entity.*;

public abstract class AbstractRestController {

	/**
	 * Lien vers la page de connexion.
	 */
	protected static final String VIEW_CONNECTION = "/accueil";
	/**
	 * Lien vers la page principale.
	 */
	protected static final String VIEW_HOME = "/";

	/**
	 * Nom de l'attribut représentant le tuteur pour la session.
	 */
	protected static final String ATT_TUTOR = "tutor";
	/**
	 * Nom de l'attribut représentant l'autiste pour la session.
	 */
	protected static final String ATT_AUTISTIC = "autistic";

	/**
	 * Nom de l'attribut représentant le message d'erreur pour les retours.
	 */
	protected static final String ATT_MSG_ERROR = "errorMsg";

	/**
	 * Message par défaut lorsqu'une session n'est pas connectée.
	 */
	protected static final String MSG_UNCONNECTED = "Veuillez vous connecter pour continuer.";

	/**
	 * Retourne le tuteur de la session en cours si celui-ci est connecté.
	 * 
	 * @param session
	 * @return {@link Tutor} actuellement connecté ou la valeur <code>null</code> le
	 *         cas échéant.
	 */
	protected Tutor getTutorFromSession(HttpSession session) {
		Tutor tutor = null;

		try {
			tutor = (Tutor) session.getAttribute(ATT_TUTOR);
		} catch (Exception e) {
			manageError(e, "Erreur lors de la récupération du tuteur connecté.");
		}

		return tutor;
	}

	/**
	 * Retourne l'autiste de la session en cours si celui-ci est connecté.
	 * 
	 * @param session
	 * @return {@link Tutor} actuellement connecté ou la valeur <code>null</code> le
	 *         cas échéant.
	 */
	protected Autism getAutisticFromSession(HttpSession session) {
		Autism autistic = null;

		try {
			autistic = (Autism) session.getAttribute(ATT_AUTISTIC);
		} catch (Exception e) {
			manageError(e, "Erreur lors de la récupération de l'autiste connecté.");
		}

		return autistic;
	}

	/**
	 * Contrôle si un tuteur est connecté.
	 * 
	 * @param session
	 * @return <code>true</code> si un tuteur est connecté; sinon
	 *         <code>false</code>.
	 */
	protected boolean isTutorConnected(HttpSession session) {
		return (getTutorFromSession(session) != null);
	}

	/**
	 * Contrôle si un autiste est connecté.
	 * 
	 * @param session
	 * @return <code>true</code> si un autiste est connecté; sinon
	 *         <code>false</code>.
	 */
	protected boolean isAutismConnected(HttpSession session) {
		return (getAutisticFromSession(session) != null);
	}

	/**
	 * 
	 * @param session
	 * @return <code>true</code> si une personne est connectée; sinon
	 *         <code>false</code>.
	 */
	protected boolean isConnected(HttpSession session) {
		return (isTutorConnected(session) || isAutismConnected(session));
	}

	/**
	 * Gestion par défaut d'une erreur.
	 * 
	 * @param e
	 *            Exception à gérer.
	 * @param errMsg
	 *            Message d'erreur complémentaire.
	 */
	protected void manageError(Exception e, String errMsg) {
		PrintStream stream = System.err;
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		Date date = new Date();

		stream.println("===");
		stream.println(dateFormat.format(date));
		stream.println("Une erreur est survenue :");
		e.printStackTrace(stream);
		if (errMsg != null) {
			stream.println("Information complémentaire :");
			stream.println(errMsg);
		}
	}
}
