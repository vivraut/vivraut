package com.vivraut.rest;

import java.io.IOException;
import java.text.ParseException;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vivraut.entity.*;
import com.vivraut.dao.*;
import com.vivraut.dto.*;

import org.apache.ode.utils.ISO8601DateParser;

@RestController
public class PlanningRestController extends AbstractRestController {

	@Autowired
	AutismRepository autRepo;

	@Autowired
	ActivityRepository actRepo;

	@Autowired
	ScheduleRepository schRepo;

	@Autowired
	TutorRepository tutRepo;

	private ModelMapper modelMapper = new ModelMapper();

	/**
	 * Récupération du planning.
	 * 
	 * @throws ParseException
	 */
	@RequestMapping(value = "/planning/{idAutistic}", method = RequestMethod.GET, headers = "Accept=application/json")
	protected List<ScheduleDto> read(@PathVariable Integer idAutistic, @RequestParam(required = false) String startDate,
			@RequestParam(required = false) String endDate, HttpSession session) {
		List<Schedule> planning = null;
		List<ScheduleDto> schDto = null;

		if (isConnected(session)) {
			boolean canShow = false;
			Autism aut = getAutisticFromSession(session);
			Tutor tut = getTutorFromSession(session);
			if (aut != null) {
				canShow = (aut.getIdAutism() == idAutistic);
			} else if (tut != null) {
				tut = tutRepo.findOne(tut.getIdTutor());
				List<Autism> la = tut.getAutisms();
				for (Autism a : la) {
					canShow |= (a.getIdAutism() == idAutistic);
				}
			}

			if (canShow) {
				Autism a = autRepo.findOne(idAutistic);
				planning = a.getSchedules();

				schDto = new ArrayList<ScheduleDto>();
				if (planning != null) {
					boolean add;
					Date startActivity, endActivity;
					for (Schedule s : planning) {
						Date start = null;
						try {
							// TODO Ne fonctionne pas pour le moment
							start = ISO8601DateParser.parse(startDate);
						} catch (Exception e) {
							// e.printStackTrace();
						}
						startActivity = s.getStartdate();
						add = (start == null || startActivity == null || !(start.after(startActivity)));

						Date end = null;
						try {
							// TODO Ne fonctionne pas pour le moment
							end = ISO8601DateParser.parse(endDate);
						} catch (Exception e) {
							// e.printStackTrace();
						}
						endActivity = s.getEnddate();
						add &= (end == null || endActivity == null || !(end.before(endActivity)));

						if (add) {
							schDto.add(modelMapper.map(s, ScheduleDto.class));
						}
					}
				}
			}
		}

		return schDto;
	}

	/**
	 * Mise à jour d'un élément du planning.
	 */
	@RequestMapping(value = "/planning", method = RequestMethod.POST, headers = "Accept=application/json")
	protected void update(HttpSession session) throws ServletException, IOException {
		if (isTutorConnected(session)) {
			// TODO
		}
	}

	/**
	 * Suppression d'un élément du planning.
	 */
	@RequestMapping(value = "/planning", method = RequestMethod.DELETE, headers = "Accept=application/json")
	protected void delete(HttpSession session, @RequestParam String idSchedule) {
		if (isConnected(session)) {
			System.out.println("Suppression d'une ligne dans la table Schedule (id=" + idSchedule + ").");
			int id = Integer.parseInt(idSchedule);
			schRepo.delete(id);
		}
	}

	/**
	 * Ajout d'un élément au planning.
	 * @throws ParseException 
	 */
	@RequestMapping(value = "/planning", method = RequestMethod.PUT, headers = "Accept=application/json")
	protected ScheduleDto create(HttpSession session, @RequestParam(required=false) String startDate, @RequestParam(required=false) String endDate, @RequestParam String idAutism, @RequestParam String idActivity) throws ParseException {
		ScheduleDto dto = null;
		
		if (isConnected(session)) {
			System.out.println("Ajout d'une ligne au planning...\nDonnées reçues :");
			System.out.println("startDate = " + startDate);
			System.out.println("endDate = " + endDate);
			System.out.println("idAutism = " + idAutism);
			System.out.println("idActivity = " + idActivity);
			
			int idAut = Integer.parseInt(idAutism);
			int idAct = Integer.parseInt(idActivity);
			Activity activity = actRepo.findOne(idAct);
			Autism autism = autRepo.findOne(idAut);
			Date start = ISO8601DateParser.parse(startDate);
			Date end = ISO8601DateParser.parse(endDate);
			
			Schedule schedule = new Schedule();
			schedule.setStartdate(start);
			schedule.setEnddate(end);
			schedule.setActivity(activity);
			schedule.setAutism(autism);
			
			Schedule updatedSchedule = schRepo.save(schedule);
			dto = modelMapper.map(updatedSchedule, ScheduleDto.class);
		}
		
		return dto;
	}
}
