package com.vivraut.rest;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.vivraut.entity.*;
import com.vivraut.dao.*;
import com.vivraut.dto.*;

@RestController
public class ActivityRestController extends AbstractRestController {

	@Autowired
	ActivityRepository actRepo;

	@Autowired
	AutismRepository autRepo;
	
	private ModelMapper modelMapper = new ModelMapper();

	@RequestMapping(value = "/activities/{idAutism}", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<ActivityDto> getActivities(@PathVariable Integer idAutism, HttpSession session) {
		List<Activity> activities = null;
		if (isConnected(session)) {
			Autism autism = autRepo.findOne(idAutism);
			activities = actRepo.findByAutism(autism);
		}
		
		List<ActivityDto> actDto = new ArrayList<>();
		if (activities != null) {
			activities.forEach(a -> actDto.add(modelMapper.map(a, ActivityDto.class)));
		}
		return actDto;
	}
	
	@RequestMapping(value = "/activity/{idActivity}", method = RequestMethod.GET, headers = "Accept=application/json")
	public ActivityDto getActivityDetails(@PathVariable Integer idActivity, HttpSession session) {
		Activity activity = null;
		if (isConnected(session)) {
			activity = actRepo.findOne(idActivity);
		}
		
		ActivityDto actDto = null;
		if (activity != null) {
			actDto = modelMapper.map(activity, ActivityDto.class);
		}
		return actDto;
	}
}
