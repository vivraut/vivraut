package com.vivraut.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.vivraut.entity.*;

public interface ActivityRepository extends CrudRepository<Activity, Integer> {

	List<Activity> findByAutism(Autism autism);
}
