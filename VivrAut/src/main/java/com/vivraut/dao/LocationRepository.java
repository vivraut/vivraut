package com.vivraut.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.vivraut.entity.*;

public interface LocationRepository extends CrudRepository<Location, Integer> {
	List<Location> findByTutor(Tutor tutor);
}
