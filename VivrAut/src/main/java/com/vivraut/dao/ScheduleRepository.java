package com.vivraut.dao;

import org.springframework.data.repository.CrudRepository;

import com.vivraut.entity.*;

public interface ScheduleRepository extends CrudRepository<Schedule, Integer> {

}
