package com.vivraut.dao;

import org.springframework.data.repository.CrudRepository;

import com.vivraut.entity.*;

public interface TutorRepository extends CrudRepository<Tutor, Integer> {
	
	Tutor findOneByMailTutorAndPasswordTutor(String mailTutor, String passwordTutor);
	Tutor findOneByMailTutorAndFirstnameTutor(String mailTutor, String lastnameTutor);
}
