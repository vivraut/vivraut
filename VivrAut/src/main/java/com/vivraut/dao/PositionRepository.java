package com.vivraut.dao;

import org.springframework.data.repository.CrudRepository;

import com.vivraut.entity.*;

public interface PositionRepository extends CrudRepository<Position, Integer> {

}
