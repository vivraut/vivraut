package com.vivraut.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.vivraut.entity.*;

public interface AutismRepository extends CrudRepository<Autism, Integer> {

	List<Autism> findByTutor(Tutor tutor);
}
