package com.vivraut.entity;

import java.math.BigInteger;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2017-09-13T02:50:00.509+0200")
@StaticMetamodel(Autism.class)
public class Autism_ {
	public static volatile SingularAttribute<Autism, Integer> idAutism;
	public static volatile SingularAttribute<Autism, String> firstnameAutism;
	public static volatile SingularAttribute<Autism, BigInteger> imeiAutism;
	public static volatile SingularAttribute<Autism, String> lastnameAutism;
	public static volatile SingularAttribute<Autism, String> loginAutism;
	public static volatile SingularAttribute<Autism, String> mailAutism;
	public static volatile SingularAttribute<Autism, String> passwordAutism;
	public static volatile SingularAttribute<Autism, String> phoneNumberAutism;
	public static volatile ListAttribute<Autism, Activity> activities;
	public static volatile SingularAttribute<Autism, Tutor> tutor;
	public static volatile ListAttribute<Autism, Position> positions;
	public static volatile ListAttribute<Autism, Schedule> schedules;
}
