package com.vivraut.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigInteger;
import java.util.List;


/**
 * The persistent class for the autism database table.
 * 
 */
@Entity
@NamedQuery(name="Autism.findAll", query="SELECT a FROM Autism a")
public class Autism implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_AUTISM")
	private int idAutism;

	@Column(name="FIRSTNAME_AUTISM")
	private String firstnameAutism;

	@Column(name="IMEI_AUTISM")
	private BigInteger imeiAutism;

	@Column(name="LASTNAME_AUTISM")
	private String lastnameAutism;

	@Column(name="LOGIN_AUTISM")
	private String loginAutism;

	@Column(name="MAIL_AUTISM")
	private String mailAutism;

	@Column(name="PASSWORD_AUTISM")
	private String passwordAutism;

	@Column(name="PHONE_NUMBER_AUTISM")
	private String phoneNumberAutism;

	//bi-directional many-to-one association to Activity
	@OneToMany(mappedBy="autism", cascade=CascadeType.ALL)
	private List<Activity> activities;

	//bi-directional many-to-one association to Tutor
	@ManyToOne
	@JoinColumn(name="ID_TUTOR")
	private Tutor tutor;

	//bi-directional many-to-one association to Position
	@OneToMany(mappedBy="autism", cascade=CascadeType.ALL)
	private List<Position> positions;

	//bi-directional many-to-one association to Schedule
	@OneToMany(mappedBy="autism", cascade=CascadeType.ALL)
	private List<Schedule> schedules;

	public Autism() {
	}

	public Autism(String fIRSTNAME_AUTISM, BigInteger iIMEI, String lASTNAME_AUTISM, String lOGIN_AUTISM,
			String mAIL_AUTISM, String pASSWORD_AUTISM, String pHONE_NUMBER_AUTISM, Tutor tu) {
		this.firstnameAutism = fIRSTNAME_AUTISM;
		this.imeiAutism = iIMEI;
		this.lastnameAutism = lASTNAME_AUTISM;
		this.loginAutism = lOGIN_AUTISM;
		this.mailAutism = mAIL_AUTISM;
		this.passwordAutism = pASSWORD_AUTISM;
		this.phoneNumberAutism = pHONE_NUMBER_AUTISM;
		this.tutor = tu;
	}


	public int getIdAutism() {
		return this.idAutism;
	}

	public void setIdAutism(int idAutism) {
		this.idAutism = idAutism;
	}

	public String getFirstnameAutism() {
		return this.firstnameAutism;
	}

	public void setFirstnameAutism(String firstnameAutism) {
		this.firstnameAutism = firstnameAutism;
	}

	public BigInteger getImeiAutism() {
		return this.imeiAutism;
	}

	public void setImeiAutism(BigInteger imeiAutism) {
		this.imeiAutism = imeiAutism;
	}

	public String getLastnameAutism() {
		return this.lastnameAutism;
	}

	public void setLastnameAutism(String lastnameAutism) {
		this.lastnameAutism = lastnameAutism;
	}

	public String getLoginAutism() {
		return this.loginAutism;
	}

	public void setLoginAutism(String loginAutism) {
		this.loginAutism = loginAutism;
	}

	public String getMailAutism() {
		return this.mailAutism;
	}

	public void setMailAutism(String mailAutism) {
		this.mailAutism = mailAutism;
	}

	public String getPasswordAutism() {
		return this.passwordAutism;
	}

	public void setPasswordAutism(String passwordAutism) {
		this.passwordAutism = passwordAutism;
	}

	public String getPhoneNumberAutism() {
		return this.phoneNumberAutism;
	}

	public void setPhoneNumberAutism(String phoneNumberAutism) {
		this.phoneNumberAutism = phoneNumberAutism;
	}

	public List<Activity> getActivities() {
		return this.activities;
	}

	public void setActivities(List<Activity> activities) {
		this.activities = activities;
	}

	public Activity addActivity(Activity activity) {
		getActivities().add(activity);
		activity.setAutism(this);

		return activity;
	}

	public Activity removeActivity(Activity activity) {
		getActivities().remove(activity);
		activity.setAutism(null);

		return activity;
	}

	public Tutor getTutor() {
		return this.tutor;
	}

	public void setTutor(Tutor tutor) {
		this.tutor = tutor;
	}

	public List<Position> getPositions() {
		return this.positions;
	}

	public void setPositions(List<Position> positions) {
		this.positions = positions;
	}

	public Position addPosition(Position position) {
		getPositions().add(position);
		position.setAutism(this);

		return position;
	}

	public Position removePosition(Position position) {
		getPositions().remove(position);
		position.setAutism(null);

		return position;
	}

	public List<Schedule> getSchedules() {
		return this.schedules;
	}

	public void setSchedules(List<Schedule> schedules) {
		this.schedules = schedules;
	}

	public Schedule addSchedule(Schedule schedule) {
		getSchedules().add(schedule);
		schedule.setAutism(this);

		return schedule;
	}

	public Schedule removeSchedule(Schedule schedule) {
		getSchedules().remove(schedule);
		schedule.setAutism(null);

		return schedule;
	}

}