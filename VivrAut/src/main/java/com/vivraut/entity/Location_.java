package com.vivraut.entity;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2017-09-13T02:50:00.592+0200")
@StaticMetamodel(Location.class)
public class Location_ {
	public static volatile SingularAttribute<Location, Integer> idLocation;
	public static volatile SingularAttribute<Location, String> addressLocation;
	public static volatile SingularAttribute<Location, String> cityLocation;
	public static volatile SingularAttribute<Location, BigDecimal> gitudeLocation;
	public static volatile SingularAttribute<Location, BigDecimal> latitudeLocation;
	public static volatile SingularAttribute<Location, String> nameLocation;
	public static volatile SingularAttribute<Location, String> postalCodeLocation;
	public static volatile ListAttribute<Location, Activity> activities;
	public static volatile SingularAttribute<Location, Tutor> tutor;
}
