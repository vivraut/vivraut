package com.vivraut.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2017-09-13T02:50:00.638+0200")
@StaticMetamodel(Tutor.class)
public class Tutor_ {
	public static volatile SingularAttribute<Tutor, Integer> idTutor;
	public static volatile SingularAttribute<Tutor, String> firstnameTutor;
	public static volatile SingularAttribute<Tutor, String> lastnameTutor;
	public static volatile SingularAttribute<Tutor, String> mailTutor;
	public static volatile SingularAttribute<Tutor, String> passwordTutor;
	public static volatile SingularAttribute<Tutor, String> phoneNumberTutor;
	public static volatile ListAttribute<Tutor, Autism> autisms;
	public static volatile ListAttribute<Tutor, Location> locations;
}
