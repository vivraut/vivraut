package com.vivraut.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tutor database table.
 * 
 */
@Entity
@NamedQuery(name="Tutor.findAll", query="SELECT t FROM Tutor t")
public class Tutor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_TUTOR")
	private int idTutor;

	@Column(name="FIRSTNAME_TUTOR")
	private String firstnameTutor;

	@Column(name="LASTNAME_TUTOR")
	private String lastnameTutor;

	@Column(name="MAIL_TUTOR")
	private String mailTutor;

	@Column(name="PASSWORD_TUTOR")
	private String passwordTutor;

	@Column(name="PHONE_NUMBER_TUTOR")
	private String phoneNumberTutor;

	//bi-directional many-to-one association to Autism
	@OneToMany(mappedBy="tutor")
	private List<Autism> autisms;

	//bi-directional many-to-one association to Location
	@OneToMany(mappedBy="tutor")
	private List<Location> locations;

	public Tutor() {
	}

	public int getIdTutor() {
		return this.idTutor;
	}

	public void setIdTutor(int idTutor) {
		this.idTutor = idTutor;
	}

	public String getFirstnameTutor() {
		return this.firstnameTutor;
	}

	public void setFirstnameTutor(String firstnameTutor) {
		this.firstnameTutor = firstnameTutor;
	}

	public String getLastnameTutor() {
		return this.lastnameTutor;
	}

	public void setLastnameTutor(String lastnameTutor) {
		this.lastnameTutor = lastnameTutor;
	}

	public String getMailTutor() {
		return this.mailTutor;
	}

	public void setMailTutor(String mailTutor) {
		this.mailTutor = mailTutor;
	}

	public String getPasswordTutor() {
		return this.passwordTutor;
	}

	public void setPasswordTutor(String passwordTutor) {
		this.passwordTutor = passwordTutor;
	}

	public String getPhoneNumberTutor() {
		return this.phoneNumberTutor;
	}

	public void setPhoneNumberTutor(String phoneNumberTutor) {
		this.phoneNumberTutor = phoneNumberTutor;
	}

	public List<Autism> getAutisms() {
		return this.autisms;
	}

	public void setAutisms(List<Autism> autisms) {
		this.autisms = autisms;
	}

	public Autism addAutism(Autism autism) {
		getAutisms().add(autism);
		autism.setTutor(this);

		return autism;
	}

	public Autism removeAutism(Autism autism) {
		getAutisms().remove(autism);
		autism.setTutor(null);

		return autism;
	}

	public List<Location> getLocations() {
		return this.locations;
	}

	public void setLocations(List<Location> locations) {
		this.locations = locations;
	}

	public Location addLocation(Location location) {
		getLocations().add(location);
		location.setTutor(this);

		return location;
	}

	public Location removeLocation(Location location) {
		getLocations().remove(location);
		location.setTutor(null);

		return location;
	}

}