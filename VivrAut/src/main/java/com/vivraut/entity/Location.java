package com.vivraut.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

/**
 * The persistent class for the location database table.
 * 
 */
@Entity
@NamedQuery(name = "Location.findAll", query = "SELECT l FROM Location l")
public class Location implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_LOCATION")
	private int idLocation;

	@Column(name = "ADDRESS_LOCATION")
	private String addressLocation;

	@Column(name = "CITY_LOCATION")
	private String cityLocation;

	@Column(name = "GITUDE_LOCATION")
	private BigDecimal gitudeLocation;

	@Column(name = "LATITUDE_LOCATION")
	private BigDecimal latitudeLocation;

	@Column(name = "NAME_LOCATION")
	private String nameLocation;

	@Column(name = "POSTAL_CODE_LOCATION")
	private String postalCodeLocation;

	// bi-directional many-to-one association to Activity
	@OneToMany(mappedBy = "location")
	private List<Activity> activities;

	// bi-directional many-to-one association to Tutor
	@ManyToOne
	private Tutor tutor;

	public Location() {
	}

	public Location(String nameLocation, String addressLocation, String postalCodeLocation, String cityLocation,
			BigDecimal gitudeLocation, BigDecimal latitudeLocation, Tutor tutor) {
		super();
		this.nameLocation = nameLocation;
		this.addressLocation = addressLocation;
		this.postalCodeLocation = postalCodeLocation;
		this.cityLocation = cityLocation;
		this.gitudeLocation = gitudeLocation;
		this.latitudeLocation = latitudeLocation;
		this.tutor = tutor;
	}

	public int getIdLocation() {
		return this.idLocation;
	}

	public void setIdLocation(int idLocation) {
		this.idLocation = idLocation;
	}

	public String getAddressLocation() {
		return this.addressLocation;
	}

	public void setAddressLocation(String addressLocation) {
		this.addressLocation = addressLocation;
	}

	public String getCityLocation() {
		return this.cityLocation;
	}

	public void setCityLocation(String cityLocation) {
		this.cityLocation = cityLocation;
	}

	public BigDecimal getGitudeLocation() {
		return this.gitudeLocation;
	}

	public void setGitudeLocation(BigDecimal gitudeLocation) {
		this.gitudeLocation = gitudeLocation;
	}

	public BigDecimal getLatitudeLocation() {
		return this.latitudeLocation;
	}

	public void setLatitudeLocation(BigDecimal latitudeLocation) {
		this.latitudeLocation = latitudeLocation;
	}

	public String getNameLocation() {
		return this.nameLocation;
	}

	public void setNameLocation(String nameLocation) {
		this.nameLocation = nameLocation;
	}

	public String getPostalCodeLocation() {
		return this.postalCodeLocation;
	}

	public void setPostalCodeLocation(String postalCodeLocation) {
		this.postalCodeLocation = postalCodeLocation;
	}

	public List<Activity> getActivities() {
		return this.activities;
	}

	public void setActivities(List<Activity> activities) {
		this.activities = activities;
	}

	public Activity addActivity(Activity activity) {
		getActivities().add(activity);
		activity.setLocation(this);

		return activity;
	}

	public Activity removeActivity(Activity activity) {
		getActivities().remove(activity);
		activity.setLocation(null);

		return activity;
	}

	public Tutor getTutor() {
		return this.tutor;
	}

	public void setTutor(Tutor tutor) {
		this.tutor = tutor;
	}

}