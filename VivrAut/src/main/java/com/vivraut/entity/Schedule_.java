package com.vivraut.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2017-09-13T02:50:00.622+0200")
@StaticMetamodel(Schedule.class)
public class Schedule_ {
	public static volatile SingularAttribute<Schedule, Integer> idSchedule;
	public static volatile SingularAttribute<Schedule, Date> enddate;
	public static volatile SingularAttribute<Schedule, Date> startdate;
	public static volatile SingularAttribute<Schedule, Activity> activity;
	public static volatile SingularAttribute<Schedule, Autism> autism;
}
