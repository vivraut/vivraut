package com.vivraut.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2017-09-13T02:50:00.457+0200")
@StaticMetamodel(Activity.class)
public class Activity_ {
	public static volatile SingularAttribute<Activity, Integer> idActivity;
	public static volatile SingularAttribute<Activity, Integer> defaultSecondDurationActivity;
	public static volatile SingularAttribute<Activity, String> nameActivity;
	public static volatile SingularAttribute<Activity, String> urlPictureActivity;
	public static volatile SingularAttribute<Activity, Autism> autism;
	public static volatile SingularAttribute<Activity, Location> location;
	public static volatile ListAttribute<Activity, Schedule> schedules;
}
