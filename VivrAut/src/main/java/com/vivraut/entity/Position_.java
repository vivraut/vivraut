package com.vivraut.entity;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2017-09-13T02:50:00.607+0200")
@StaticMetamodel(Position.class)
public class Position_ {
	public static volatile SingularAttribute<Position, Integer> idPosition;
	public static volatile SingularAttribute<Position, Date> date;
	public static volatile SingularAttribute<Position, BigDecimal> latitudePosition;
	public static volatile SingularAttribute<Position, BigDecimal> longitudePosition;
	public static volatile SingularAttribute<Position, Autism> autism;
}
