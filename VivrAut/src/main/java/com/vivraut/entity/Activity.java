package com.vivraut.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the activity database table.
 * 
 */
@Entity
@NamedQuery(name="Activity.findAll", query="SELECT a FROM Activity a")
public class Activity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_ACTIVITY")
	private int idActivity;

	@Column(name="DEFAULT_SECOND_DURATION_ACTIVITY")
	private Integer defaultSecondDurationActivity;

	@Column(name="NAME_ACTIVITY")
	private String nameActivity;

	@Column(name="URL_PICTURE_ACTIVITY")
	private String urlPictureActivity;

	//bi-directional many-to-one association to Autism
	@ManyToOne
	private Autism autism;

	//bi-directional many-to-one association to Location
	@ManyToOne(cascade=CascadeType.ALL)
	private Location location;

	//bi-directional many-to-one association to Schedule
	@OneToMany(mappedBy="activity")
	private List<Schedule> schedules;

	public Activity() {
	}

	public int getIdActivity() {
		return this.idActivity;
	}

	public void setIdActivity(int idActivity) {
		this.idActivity = idActivity;
	}

	public Integer getDefaultSecondDurationActivity() {
		return this.defaultSecondDurationActivity;
	}

	public void setDefaultSecondDurationActivity(Integer defaultSecondDurationActivity) {
		this.defaultSecondDurationActivity = defaultSecondDurationActivity;
	}

	public String getNameActivity() {
		return this.nameActivity;
	}

	public void setNameActivity(String nameActivity) {
		this.nameActivity = nameActivity;
	}

	public String getUrlPictureActivity() {
		return this.urlPictureActivity;
	}

	public void setUrlPictureActivity(String urlPictureActivity) {
		this.urlPictureActivity = urlPictureActivity;
	}

	public Autism getAutism() {
		return this.autism;
	}

	public void setAutism(Autism autism) {
		this.autism = autism;
	}

	public Location getLocation() {
		return this.location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public List<Schedule> getSchedules() {
		return this.schedules;
	}

	public void setSchedules(List<Schedule> schedules) {
		this.schedules = schedules;
	}

	public Schedule addSchedule(Schedule schedule) {
		getSchedules().add(schedule);
		schedule.setActivity(this);

		return schedule;
	}

	public Schedule removeSchedule(Schedule schedule) {
		getSchedules().remove(schedule);
		schedule.setActivity(null);

		return schedule;
	}

}