package com.vivraut.dto;

import java.io.Serializable;
import java.math.BigInteger;

public class AutismDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private int idAutism;

	private String firstnameAutism;

	private BigInteger imeiAutism;

	private String lastnameAutism;

	private String loginAutism;

	private String mailAutism;

	//private String passwordAutism;

	private String phoneNumberAutism;
	
	public AutismDto() {
	}

	public int getIdAutism() {
		return this.idAutism;
	}

	public void setIdAutism(int idAutism) {
		this.idAutism = idAutism;
	}

	public String getFirstnameAutism() {
		return this.firstnameAutism;
	}

	public void setFirstnameAutism(String firstnameAutism) {
		this.firstnameAutism = firstnameAutism;
	}

	public BigInteger getImeiAutism() {
		return this.imeiAutism;
	}

	public void setImeiAutism(BigInteger imeiAutism) {
		this.imeiAutism = imeiAutism;
	}

	public String getLastnameAutism() {
		return this.lastnameAutism;
	}

	public void setLastnameAutism(String lastnameAutism) {
		this.lastnameAutism = lastnameAutism;
	}

	public String getLoginAutism() {
		return this.loginAutism;
	}

	public void setLoginAutism(String loginAutism) {
		this.loginAutism = loginAutism;
	}

	public String getMailAutism() {
		return this.mailAutism;
	}

	public void setMailAutism(String mailAutism) {
		this.mailAutism = mailAutism;
	}

	/*public String getPasswordAutism() {
		return this.passwordAutism;
	}

	public void setPasswordAutism(String passwordAutism) {
		this.passwordAutism = passwordAutism;
	}*/

	public String getPhoneNumberAutism() {
		return this.phoneNumberAutism;
	}

	public void setPhoneNumberAutism(String phoneNumberAutism) {
		this.phoneNumberAutism = phoneNumberAutism;
	}
}