package com.vivraut.dto;

import java.io.Serializable;

public class ActivityDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private int idActivity;

	private Integer defaultSecondDurationActivity;

	private String nameActivity;

	private String urlPictureActivity;

	private LocationDto location;

	public ActivityDto() {
	}

	public int getIdActivity() {
		return this.idActivity;
	}

	public void setIdActivity(int idActivity) {
		this.idActivity = idActivity;
	}

	public Integer getDefaultSecondDurationActivity() {
		return this.defaultSecondDurationActivity;
	}

	public void setDefaultSecondDurationActivity(Integer defaultSecondDurationActivity) {
		this.defaultSecondDurationActivity = defaultSecondDurationActivity;
	}

	public String getNameActivity() {
		return this.nameActivity;
	}

	public void setNameActivity(String nameActivity) {
		this.nameActivity = nameActivity;
	}

	public String getUrlPictureActivity() {
		return this.urlPictureActivity;
	}

	public void setUrlPictureActivity(String urlPictureActivity) {
		this.urlPictureActivity = urlPictureActivity;
	}

	public LocationDto getLocation() {
		return this.location;
	}

	public void setLocation(LocationDto location) {
		this.location = location;
	}
}
