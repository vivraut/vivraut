package com.vivraut.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class LocationDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private int idLocation;

	private String addressLocation;

	private String cityLocation;

	private BigDecimal gitudeLocation;

	private BigDecimal latitudeLocation;

	private String nameLocation;

	private String postalCodeLocation;

	public LocationDto() {
	}

	public int getIdLocation() {
		return this.idLocation;
	}

	public void setIdLocation(int idLocation) {
		this.idLocation = idLocation;
	}

	public String getAddressLocation() {
		return this.addressLocation;
	}

	public void setAddressLocation(String addressLocation) {
		this.addressLocation = addressLocation;
	}

	public String getCityLocation() {
		return this.cityLocation;
	}

	public void setCityLocation(String cityLocation) {
		this.cityLocation = cityLocation;
	}

	public BigDecimal getGitudeLocation() {
		return this.gitudeLocation;
	}

	public void setGitudeLocation(BigDecimal gitudeLocation) {
		this.gitudeLocation = gitudeLocation;
	}

	public BigDecimal getLatitudeLocation() {
		return this.latitudeLocation;
	}

	public void setLatitudeLocation(BigDecimal latitudeLocation) {
		this.latitudeLocation = latitudeLocation;
	}

	public String getNameLocation() {
		return this.nameLocation;
	}

	public void setNameLocation(String nameLocation) {
		this.nameLocation = nameLocation;
	}

	public String getPostalCodeLocation() {
		return this.postalCodeLocation;
	}

	public void setPostalCodeLocation(String postalCodeLocation) {
		this.postalCodeLocation = postalCodeLocation;
	}
}
