package com.vivraut.dto;

import java.io.Serializable;
import java.util.Date;

public class ScheduleDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private int idSchedule;

	private Date enddate;

	private Date startdate;

	private ActivityDto activity;

	public ScheduleDto() {
	}

	public int getIdSchedule() {
		return this.idSchedule;
	}

	public void setIdSchedule(int idSchedule) {
		this.idSchedule = idSchedule;
	}

	public Date getEnddate() {
		return this.enddate;
	}

	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}

	public Date getStartdate() {
		return this.startdate;
	}

	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	public ActivityDto getActivity() {
		return this.activity;
	}

	public void setActivity(ActivityDto activity) {
		this.activity = activity;
	}
}
