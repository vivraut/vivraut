package com.vivraut.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class PositionDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private int idPosition;

	private Date date;

	private BigDecimal latitudePosition;

	private BigDecimal longitudePosition;

	public PositionDto() {
	}

	public int getIdPosition() {
		return this.idPosition;
	}

	public void setIdPosition(int idPosition) {
		this.idPosition = idPosition;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public BigDecimal getLatitudePosition() {
		return this.latitudePosition;
	}

	public void setLatitudePosition(BigDecimal latitudePosition) {
		this.latitudePosition = latitudePosition;
	}

	public BigDecimal getLongitudePosition() {
		return this.longitudePosition;
	}

	public void setLongitudePosition(BigDecimal longitudePosition) {
		this.longitudePosition = longitudePosition;
	}
}
