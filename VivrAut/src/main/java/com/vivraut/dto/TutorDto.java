package com.vivraut.dto;

import java.io.Serializable;

public class TutorDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private int idTutor;

	private String firstnameTutor;

	private String lastnameTutor;

	private String mailTutor;

	private String passwordTutor;

	private String phoneNumberTutor;

	public TutorDto() {
	}

	public int getIdTutor() {
		return this.idTutor;
	}

	public void setIdTutor(int idTutor) {
		this.idTutor = idTutor;
	}

	public String getFirstnameTutor() {
		return this.firstnameTutor;
	}

	public void setFirstnameTutor(String firstnameTutor) {
		this.firstnameTutor = firstnameTutor;
	}

	public String getLastnameTutor() {
		return this.lastnameTutor;
	}

	public void setLastnameTutor(String lastnameTutor) {
		this.lastnameTutor = lastnameTutor;
	}

	public String getMailTutor() {
		return this.mailTutor;
	}

	public void setMailTutor(String mailTutor) {
		this.mailTutor = mailTutor;
	}

	public String getPasswordTutor() {
		return this.passwordTutor;
	}

	public void setPasswordTutor(String passwordTutor) {
		this.passwordTutor = passwordTutor;
	}

	public String getPhoneNumberTutor() {
		return this.phoneNumberTutor;
	}

	public void setPhoneNumberTutor(String phoneNumberTutor) {
		this.phoneNumberTutor = phoneNumberTutor;
	}
}
