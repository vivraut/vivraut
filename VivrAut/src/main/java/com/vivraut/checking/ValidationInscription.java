package com.vivraut.checking;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.vivraut.entity.*;

public class ValidationInscription {
	private static final String CHAMP_EMAIL = "email";
	private static final String CHAMP_MOTDEPASSE = "motdepasse";
	private static final String CHAMP_PRENOM = "prenom";
	private static final String CHAMP_NOM = "nom";
	private static final String CHAMP_INDTEL = "indtel";
	private static final String CHAMP_TEL = "tel";

	private String resultat;
	private Map<String, String> erreurs = new HashMap<String, String>();

	public Map<String, String> getErreurs() {
		return erreurs;
	}

	public String getResultat() {
		return resultat;
	}

	public Tutor creerTuteur(HttpServletRequest request) {
		String email = getValeurChamp(request, CHAMP_EMAIL);
		String motdepasse = getValeurChamp(request, CHAMP_MOTDEPASSE);
		String prenom = getValeurChamp(request, CHAMP_PRENOM);
		String nom = getValeurChamp(request, CHAMP_NOM);
		String indtel = getValeurChamp(request, CHAMP_INDTEL);
		String tel = getValeurChamp(request, CHAMP_TEL);

		Tutor tutor = new Tutor();

		try {
			validationEmail(email);
		} catch (Exception e) {
			setErreur(CHAMP_EMAIL, e.getMessage());
		}
		tutor.setMailTutor(email);

		try {
			validationNom(nom);
		} catch (Exception e) {
			setErreur(CHAMP_NOM, e.getMessage());
		}
		tutor.setLastnameTutor(nom);

		try {
			validationPrenom(prenom);
		} catch (Exception e) {
			setErreur(CHAMP_PRENOM, e.getMessage());
		}
		tutor.setFirstnameTutor(prenom);

		try {
			validationTelephone(tel);
		} catch (Exception e) {
			setErreur(CHAMP_TEL, e.getMessage());
		}
		tutor.setPhoneNumberTutor(tel);

		try {
			validationMotDePasse(motdepasse);
		} catch (Exception e) {
			setErreur(CHAMP_MOTDEPASSE, e.getMessage());
		}
		tutor.setPasswordTutor(motdepasse);
		
		try {
			validationIndTel(indtel, tutor);
		} catch (Exception e) {
			setErreur(CHAMP_INDTEL, e.getMessage());
		}
		return tutor;
	}

	private void validationEmail(String email) throws Exception {
		if (email != null){
			if(!email.matches("^[a-z0-9._-]+@[a-z0-9._-]{2,}\\.[a-z]{2,4}$")) 
				throw new Exception("Merci de saisir une adresse mail valide.");
		} else
			throw new Exception("Vous devez saisir une adresse mail.");
	}
	
	private void validationMotDePasse(String motdepasse) throws Exception {
		if (motdepasse != null){
			if(motdepasse.length() < 6)
				throw new Exception("Le mot de passe doit contenir au moins 6 caract�res.");
		} else
			throw new Exception("Vous devez saisir un mot de passe d'au moins 6 caract�res.");
		
	}
	
	private void validationPrenom(String prenom) throws Exception {
		if (prenom != null){
			if (prenom.length() < 2)
				throw new Exception("Le pr�nom d'utilisateur doit contenir au moins 2 caract�res.");
		} else
			throw new Exception("Vous devez saisir un pr�nom d'au moins 2 caract�res.");
	}
	
	private void validationNom(String nom) throws Exception {
		if (nom != null && nom.length() < 2) 
			throw new Exception("Le nom d'utilisateur doit contenir au moins 2 caract�res.");
	}
	
	private void validationIndTel(String indtel, Tutor tutor) throws Exception {
		if (indtel != null && !indtel.matches("^[0-9]{1,5}")) {
			throw new Exception("L'indicatif ne doit contenir que des chiffres.");
		}
	}

	private void validationTelephone(String telephone) throws Exception {
		if (telephone != null && !telephone.matches("^\\d+$")) {
			throw new Exception("Le num�ro de t�l�phone doit uniquement contenir des chiffres.");
		} else if (telephone != null && telephone.length() < 4) {
			throw new Exception("Le num�ro de t�l�phone doit contenir au moins 4 chiffres.");
		}
	}

	

	private void setErreur(String champ, String message) {
		erreurs.put(champ, message);
	}

	private static String getValeurChamp(HttpServletRequest request, String nomChamp) {
		String valeur = request.getParameter(nomChamp);
		if (valeur == "" || valeur == null || valeur.trim().length() == 0) {
			return null;
		} else {
			return valeur;
		}
	}
}
