package com.vivraut.servlet;

import java.io.IOException;
import java.io.Writer;
import java.util.Enumeration;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class Debug {

	//@ResponseBody
	@RequestMapping("/debugSession")
	public void listSession(HttpSession session, Writer responseWriter) throws IOException {
		/*System.out.println("Elements contenu dans la session :");
		System.out.println("Identifiant = " + session.getId());
		Enumeration<String> list = session.getAttributeNames();
		while (list.hasMoreElements()) {
			String s = list.nextElement();
			System.out.println("=> " + s + " [" + session.getAttribute(s) + "]");
		}
		
		return "Les informations ont été affichées dans la console du serveur.";*/
		
		responseWriter.write("Elements contenu dans la session :\n");
		responseWriter.write("Identifiant = " + session.getId() + "\n");
		Enumeration<String> list = session.getAttributeNames();
		while (list.hasMoreElements()) {
			String s = list.nextElement();
			responseWriter.write("=> " + s + " [" + session.getAttribute(s) + "]\n");
		}
	}

}
