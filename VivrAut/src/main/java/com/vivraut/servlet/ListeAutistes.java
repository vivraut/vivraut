package com.vivraut.servlet;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.vivraut.dao.AutismRepository;
import com.vivraut.entity.Autism;
import com.vivraut.entity.Tutor;

@Controller
public class ListeAutistes {
	public static final String CHAMP_TUTOR = "tutor";
	public static final String VUE = "/listeAutistes";
	public static final String VUE_AJOUT = "/saisieAutiste";
	public static final String VUE_SUPP = "/supprimeAutiste";

	@Autowired
	AutismRepository autRepo;
	
	@GetMapping("/listeAutistes")
	protected String doGet(HttpServletRequest request, HttpSession session) {
		String action = request.getParameter("actionAutiste");
		System.out.println(action);
		Tutor t = (Tutor) session.getAttribute(CHAMP_TUTOR);
		
		List<Autism> lAu = autRepo.findByTutor(t);
		request.setAttribute("autistes", lAu);
		String v = String.format("%d", lAu.size());
		System.out.println(v);
		
		return VUE;
	}

	@PostMapping("/listeAutistes")
	protected String doPost(HttpServletRequest request, HttpSession session) {
		String action = request.getParameter("actionAutiste");
		System.out.println(action);
		Tutor t = (Tutor) session.getAttribute(CHAMP_TUTOR);
		System.out.println(t.getIdTutor());
		
		if (action.equals("Ajout")) {
			return VUE_AJOUT;
		}
		else if  (action.equals("Suppression")) {
			List<Autism> lAu = autRepo.findByTutor(t);
			System.out.println("Supp Nb:" + lAu.size());
			request.setAttribute("autistes", lAu);
			return VUE_SUPP;
			}
		else if(action.equals("Confirmer")){
			Integer idAutiste = Integer.parseInt(request.getParameter("choixAutiste"));
			System.out.println("Suppression idAutiste " + idAutiste);
			autRepo.delete(idAutiste);
			
			List<Autism> lAu = autRepo.findByTutor(t);
			request.setAttribute("autistes", lAu);
			return VUE;
		}
		
		//Action par défaut
		return "/accueil";

	}

}
