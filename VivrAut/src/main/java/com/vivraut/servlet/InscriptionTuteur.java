package com.vivraut.servlet;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.vivraut.dao.*;
import com.vivraut.entity.*;
import com.vivraut.checking.*;

@Controller
public class InscriptionTuteur {
	public static final String ATT_TUTEUR = "tuteur";
	public static final String ATT_FORM = "vi";

	public static final String VUE_ACCUEIL = "/accueil";
	public static final String VUE_SUCCES = "/succesInscription";
	public static final String VUE_FORM = "/inscription";

	@Autowired
	TutorRepository tutorDAO;
	
	@GetMapping("/inscription")
	protected String doGet() {
		return VUE_ACCUEIL;
	}

	@PostMapping("/inscription")
	protected String doPost(HttpServletRequest request) {
		ValidationInscription vi = new ValidationInscription();

		/* Traitement de la requête et récupération du bean en résultant */
		Tutor tutor = vi.creerTuteur(request);

        /* Ajout du bean et de l'objet métier à l'objet requête */
        request.setAttribute( ATT_TUTEUR, tutor );
        request.setAttribute( ATT_FORM, vi );

		if (vi.getErreurs().isEmpty()) {
			tutorDAO.save(tutor);
			return VUE_SUCCES;
		} else
			return VUE_FORM;
	}
}
