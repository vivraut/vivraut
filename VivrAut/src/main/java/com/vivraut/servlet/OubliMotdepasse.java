package com.vivraut.servlet;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.vivraut.dao.TutorRepository;
import com.vivraut.entity.Tutor;


@Controller
public class OubliMotdepasse {
	public static final String VUE_OUBLI = "/reinitMotdepasse";
	public static final String CHAMP_EMAIL = "email";
	public static final String CHAMP_PRENOM = "prenom";
	public static final String CHAMP_TUTOR = "tutor";
	public static final String CHAMP_ERREUR = "erreur";
	private Map<String, String> erreurs = new HashMap<String, String>();
	
	@Autowired
	TutorRepository tutorDAO;
	
	@GetMapping("/oubliMotdePasse")
	public String doGet() {
		return VUE_OUBLI;
	}

	@PostMapping("/oubliMotdePasse")
	public String doPost(@RequestParam String actionOubli, @RequestParam String email,
			@RequestParam String prenom,Map<String, Object> model, HttpServletRequest request) throws Exception {
		Tutor t;
		if (actionOubli.equals("Valider")) {
			String erreur = "L'adresse mail et/ou le prénom sont invalides !";
			
			if (prenom != null){
				if (prenom.length() < 2) {
					System.out.println("Perdu1");
					Exception e = new Exception("Le prénom d'utilisateur doit contenir au moins 2 caractères.");
					erreurs.put(CHAMP_PRENOM, e.getMessage());
				}
			} else {
				System.out.println("Perdu2");
				Exception e = new Exception("Vous devez saisir un prénom d'au moins 2 caractères.");
				erreurs.put(CHAMP_PRENOM, e.getMessage());
			}
			if (email != null){
				if(!email.matches("^[a-z0-9._-]+@[a-z0-9._-]{2,}\\.[a-z]{2,4}$")) {
					Exception e = new Exception("Merci de saisir une adresse mail valide.");
					erreurs.put(CHAMP_EMAIL, e.getMessage());
				}
			} else {
				Exception e = new Exception("Vous devez saisir une adresse mail.");
				erreurs.put(CHAMP_EMAIL, e.getMessage());
			}
			
			t = tutorDAO.findOneByMailTutorAndFirstnameTutor(email, prenom);
			if (t != null) {
				request.setAttribute("cache", false);
				request.setAttribute(CHAMP_TUTOR, t);
				//System.out.println("Gagné !");
				return VUE_OUBLI;
			} else {
				request.setAttribute("cache", true);
				//System.out.println("Perdu !");
				model.put(CHAMP_ERREUR, erreurs);
			}
		}	
		else if (actionOubli.equals("Réinitialiser")) {
			String password = request.getParameter("password"); // pas de @RequestParam car mot de passe non demandé dans la 1ère partie du formulaire
			String confPassword = request.getParameter("confPassword");
			if (password.equals(confPassword)) {
				//System.out.println("Gagné !");
				t = tutorDAO.findOneByMailTutorAndFirstnameTutor(email, prenom);
				t.setPasswordTutor(password);
				tutorDAO.save(t);
			}
			else
				System.out.println("Perdu !");
			return VUE_OUBLI;
		}
		return VUE_OUBLI;
	}
}
