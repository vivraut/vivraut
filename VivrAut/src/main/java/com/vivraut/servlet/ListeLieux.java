package com.vivraut.servlet;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.vivraut.dao.LocationRepository;
import com.vivraut.entity.Location;
import com.vivraut.entity.Tutor;

@Controller
public class ListeLieux {
	public static final String CHAMP_TUTOR = "tutor";
	public static final String VUE = "/listeLieux";
	public static final String VUE_AJOUT = "saisieLieu";
	public static final String VUE_SUPPR = "/supprimeLieu";
	
	@Autowired
	LocationRepository locRepo;
	
	
	@GetMapping("/listeLieux")
	protected String doGet(HttpServletRequest request, HttpSession session) {
		/*String action = request.getParameter("actionLieux");
		if (action.equals("Ajout"))
			return VUE_AJOUT;
		else if (action.equals("Suppression"))
			return VUE_SUPPR;*/
		Tutor t = (Tutor) session.getAttribute(CHAMP_TUTOR);
		List<Location> lo = locRepo.findByTutor(t);
		request.setAttribute("lieux", lo);
		return VUE;
	}
	
	@PostMapping("/listeLieux")
	protected String doPost(HttpServletRequest request, HttpSession session) {
		String action = request.getParameter("actionLieux");
		Tutor t = (Tutor)session.getAttribute(CHAMP_TUTOR);
		if (action.equals("Ajout"))
			return VUE_AJOUT;
		else if (action.equals("Suppression")) {
			List<Location> lo = locRepo.findByTutor(t);
			request.setAttribute("lieux", lo);
			return VUE_SUPPR;
		}
		else if (action.equals("Confirmer")) {
			Integer idLocation = Integer.parseInt(request.getParameter("choixLieu"));
			locRepo.delete(idLocation);
			List<Location> lo = locRepo.findByTutor(t);
			request.setAttribute("lieux", lo);
			return VUE;
		}
		else if (action.equals("Annuler")) {
			return VUE;
		}
		return "/accueil";
	}
}
