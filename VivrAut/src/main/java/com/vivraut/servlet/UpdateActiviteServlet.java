package com.vivraut.servlet;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.vivraut.dao.*;
import com.vivraut.entity.*;
import com.vivraut.checking.*;

@MultipartConfig(
        fileSizeThreshold   = 1024 * 1024 * 5,  // 5 MB : limite taille fichier 
        maxFileSize         = 1024 * 1024 * 50, // 50 MB
        maxRequestSize      = 1024 * 1024 * 50, // 50 MB
        location            = "C:/"
)
@Controller
public class UpdateActiviteServlet {
	
	public static final String ATT_ACTIVITE = "activite";
	public static final String ATT_LOCATION = "location";
	public static final String ATT_AUTISTE = "autism";
	public static final String ATT_TUTEUR = "tutor";
    public static final String ATT_FORM   = "va";
	public static final String VUE_SUCCES = "/listeActivites";
    public static final String VUE_FORM   = "/updateActivite";
    public static final String CHEMIN        = "chemin";
    public static final int TAILLE_TAMPON = 10240; // 10 ko
    public static final String CHAMP_URL     = "url";
    
    @Autowired
    ActivityRepository actRepo;
    
    @Autowired
    AutismRepository autRepo;
    
    @Autowired
    LocationRepository locRepo;
    
    @Autowired
    ServletContext context; 
    
    @GetMapping("/updateActivite")
    public String doGet() throws ServletException, IOException {
    	return VUE_FORM;
    }
    
    @PostMapping("/updateActivite")
    public String doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    	HttpSession session = request.getSession();
		Autism aut = (Autism) session.getAttribute(ATT_AUTISTE);
		Tutor tu = (Tutor) session.getAttribute(ATT_TUTEUR);
		Activity activite = (Activity)session.getAttribute(ATT_ACTIVITE);
		Location location = (Location)session.getAttribute(ATT_LOCATION);
		//System.out.println(aut.getFirstnameAutism());
		//System.out.println("id loc : "+location.getIdLocation());
		ValidationActivite va = new ValidationActivite();
		//Location location = null;
		if(location!=null)
			location = va.creerLocation(request,location,tu);
		else
			location = va.creerLocation(request,tu);
		activite = va.creerActivite(request, aut, location,activite);
		//activite.setIdActivity((int) session.getAttribute("id_activite"));
		
		if(location!=null && location.getIdLocation()==0){
			location = locRepo.save(location);
			activite.setLocation(location);
			//System.out.println("id loc : "+location.getIdLocation());
		}
		request.setAttribute( ATT_LOCATION, location );
		request.setAttribute( ATT_ACTIVITE, activite );
        request.setAttribute( ATT_FORM, va );
        //System.out.println(activite.getIdActivity());
        if (va.getErreurs().isEmpty()){
        	//ServiceLocationDAO sldao = new ServiceLocationDAO();
        	//sldao.ajouter(location);
        	
        	//if (activite.getUrlPictureActivity()!=null && activite.getUrlPictureActivity()!="") {
        	//Récupération de l'image
        	String chemin = context.getRealPath("/")+"img\\app\\";
        	//System.out.println(chemin);
            /*
             * Les données reçues sont multipart, on doit donc utiliser la m�thode
             * getPart() pour traiter le champ d'envoi de fichiers.
             */
            Part part = request.getPart( CHAMP_URL );
                
            /*
             * Il faut d�terminer s'il s'agit d'un champ classique 
             * ou d'un champ de type fichier : on délègue cette opération 
             * à la méthode utilitaire getNomFichier().
             */
            String nomFichier = getNomFichier( part );

            /*
             * Si la méthode a renvoyé quelque chose, il s'agit donc d'un champ
             * de type fichier (input type="file").
             */
            if ( nomFichier != null && !nomFichier.isEmpty() ) {
                String nomChamp = part.getName();
                /*
                 * Antibug pour Internet Explorer, qui transmet pour une raison
                 * mystique le chemin du fichier local à la machine du client...
                 * 
                 * Ex : C:/dossier/sous-dossier/fichier.ext
                 * 
                 * On doit donc faire en sorte de ne sélectionner que le nom et
                 * l'extension du fichier, et de se débarrasser du superflu.
                 */
                 nomFichier = nomFichier.substring( nomFichier.lastIndexOf( '/' ) + 1 )
                        .substring( nomFichier.lastIndexOf( '\\' ) + 1 );

                /* �criture du fichier sur le disque */
                ecrireFichier( part, nomFichier, chemin );
            }
            if(!nomFichier.equals(""))
            	activite.setUrlPictureActivity(nomFichier);
            /*if(location!=null)
            	actRepo.save(activite,location);
            else*/
            	actRepo.save(activite);
            	List<Autism> au = autRepo.findByTutor(tu);
                request.setAttribute("autiste", au);
                request.setAttribute("cache", true);
        	return VUE_SUCCES;
        	
        }
        else
        	return VUE_FORM;
    }
    
    private static String getNomFichier( Part part ) {
        /* Boucle sur chacun des param�tres de l'en-tête "content-disposition". */
        for ( String contentDisposition : part.getHeader( "content-disposition" ).split( ";" ) ) {
            /* Recherche de l'éventuelle présence du param�tre "filename". */
            if ( contentDisposition.trim().startsWith( "filename" ) ) {
                /*
                 * Si "filename" est présent, alors renvoi de sa valeur,
                 * c'est-à-dire du nom de fichier sans guillemets.
                 */
                return contentDisposition.substring( contentDisposition.indexOf( '=' ) + 1 ).trim().replace( "\"", "" );
            }
        }
        /* Et pour terminer, si rien n'a été trouvé... */
        return null;
    }
    
    private void ecrireFichier( Part part, String nomFichier, String chemin ) throws IOException {
        /* Prépare les flux. */
        BufferedInputStream entree = null;
        BufferedOutputStream sortie = null;
        try {
            /* Ouvre les flux. */
            entree = new BufferedInputStream( part.getInputStream(), TAILLE_TAMPON );
            sortie = new BufferedOutputStream( new FileOutputStream( new File( chemin + nomFichier ) ),
                    TAILLE_TAMPON );

            /*
             * Lit le fichier reçu et écrit son contenu dans un fichier sur le
             * disque.
             */
            byte[] tampon = new byte[TAILLE_TAMPON];
            int longueur;
            while ( ( longueur = entree.read( tampon ) ) > 0 ) {
                sortie.write( tampon, 0, longueur );
            }
        } finally {
            try {
                sortie.close();
            } catch ( IOException ignore ) {
            }
            try {
                entree.close();
            } catch ( IOException ignore ) {
            }
        }
    }
}
