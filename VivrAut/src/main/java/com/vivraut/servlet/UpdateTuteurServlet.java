package com.vivraut.servlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.vivraut.checking.ValidationInscription;
import com.vivraut.dao.TutorRepository;
import com.vivraut.entity.Tutor;

@Controller
public class UpdateTuteurServlet {
	public static final String ATT_TUTEUR = "tuteur";
    public static final String ATT_FORM   = "vi";
    
    public static final String CHAMP_TUTOR   = "tutor";

    public static final String VUE_SUCCES = "/succesConnexion";
    public static final String VUE_FORM   = "/updateTuteur";

    @Autowired
    TutorRepository tutRepo;
    
    @GetMapping("/updateTuteur")
    protected String doGet(HttpServletRequest request, HttpSession session) {
		Tutor t = (Tutor) session.getAttribute(CHAMP_TUTOR);
		request.setAttribute( ATT_TUTEUR, t );
    	return VUE_FORM;
	}
	
    @PostMapping("/updateTuteur")
	protected String doPost(HttpServletRequest request, HttpSession session) {
		Tutor t = (Tutor) session.getAttribute(CHAMP_TUTOR);
		request.setAttribute( ATT_TUTEUR, t );
		ValidationInscription vi = new ValidationInscription();

        /* Traitement de la requ�te et r�cup�ration du bean en r�sultant */
        Tutor tutor = vi.creerTuteur(request);
        tutor.setIdTutor(t.getIdTutor());
        /* Ajout du bean et de l'objet m�tier � l'objet requ�te */
        request.setAttribute( ATT_TUTEUR, tutor );
        request.setAttribute( ATT_FORM, vi );

        if (vi.getErreurs().isEmpty()){
        	tutor = tutRepo.save(tutor);
        	session.setAttribute(CHAMP_TUTOR, tutor);
        	return VUE_SUCCES;
        }
        else
        	return VUE_FORM;
	}
}
