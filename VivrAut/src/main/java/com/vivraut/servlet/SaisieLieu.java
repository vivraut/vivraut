package com.vivraut.servlet;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.vivraut.dao.LocationRepository;
import com.vivraut.dao.TutorRepository;
import com.vivraut.entity.Location;
import com.vivraut.entity.Tutor;

/**
 * Servlet implementation class SaisieLieu SI CA MARCHE JE TUE LE CHIEN EN COURS
 * DE DEV
 */

@Controller
public class SaisieLieu {
	public static final String VUE = "/listeLieux";

	public int ID_LOCATION;
	public int tutor_ID_TUTOR;
	public static final String ATT_ERREURS = "erreurs";
	public static final String ATT_RESULTAT = "resultat";
	public static final String CHAMP_TUTOR = "tutor";

	String NAME_LOCATION = null;
	String ADDRESS_LOCATION = null;
	String POSTAL_CODE_LOCATION = null;
	String CITY_LOCATION = null;
	String GITUDE_LOCATION = null;
	String LATITUDE_LOCATION = null;

	@Autowired
	TutorRepository tutRepo;

	@Autowired
	LocationRepository locRepo;

	@GetMapping("/saisieLieu")
	public String doGet() {
		/* Affichage de la page de cr�ation du lieu */
		return VUE;
	}

	@PostMapping("/saisieLieu")
	public String doPost(HttpServletRequest request, HttpSession session) {
		String resultat;
		Map<String, String> erreurs = new HashMap<String, String>();

		/* R�cup�ration des champs du formulaire. */
		NAME_LOCATION = request.getParameter("f_NAME_LOCATION");
		ADDRESS_LOCATION = request.getParameter("f_ADDRESS_LOCATION");
		POSTAL_CODE_LOCATION = request.getParameter("f_POSTAL_CODE_LOCATION");
		CITY_LOCATION = request.getParameter("f_CITY_LOCATION");
		GITUDE_LOCATION = request.getParameter("f_GITUDE_LOCATION");
		LATITUDE_LOCATION = request.getParameter("f_LATITUDE_LOCATION");

		/* Validation du champ nom. */
		try {
			validationNom(NAME_LOCATION, session);
		} catch (Exception e) {
			erreurs.put("NAME_LOCATION", e.getMessage());
		}

		/* Initialisation du r�sultat global de la validation. */
		if (erreurs.isEmpty()) {
			// ici procédure envoi dans la base
			enregistrerLieu(session);
			resultat = "Succès de l'inscription.";
		} else {
			resultat = "Echec de l'inscription.";
		}

		/* Stockage du r�sultat et des messages d'erreur dans l'objet request */
		request.setAttribute(ATT_ERREURS, erreurs);
		request.setAttribute(ATT_RESULTAT, resultat);

		/* Transmission de la paire d'objets request/response � notre JSP */
		Tutor t = (Tutor) session.getAttribute(CHAMP_TUTOR);
		List<Location> lo = locRepo.findByTutor(t);
		request.setAttribute("lieux", lo);
		return VUE;
	}

	/**
	 * Valide le nom du lieu saisi.
	 */
	private void validationNom(String NAME_LOCATION, HttpSession session) throws Exception {
		if (NAME_LOCATION != null) {
			System.out.println("Nom du lieu : " + NAME_LOCATION);
			//enregistrerLieu(session);
		} else {
			throw new Exception("La saisie du nom de lieu est obligatoire.");
		}
	}

	private void enregistrerLieu(HttpSession session) {
		Tutor tu = (Tutor) session.getAttribute(CHAMP_TUTOR);
		GITUDE_LOCATION = GITUDE_LOCATION.replace(",", ".");
		LATITUDE_LOCATION = LATITUDE_LOCATION.replace(",", ".");
		Location Lieu = new Location(NAME_LOCATION, ADDRESS_LOCATION, POSTAL_CODE_LOCATION, CITY_LOCATION,
				BigDecimal.valueOf(Double.parseDouble(GITUDE_LOCATION)),
				BigDecimal.valueOf(Double.parseDouble(LATITUDE_LOCATION)), tu);
		locRepo.save(Lieu);
	}
}