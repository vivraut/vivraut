package com.vivraut.servlet;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.vivraut.dao.AutismRepository;
import com.vivraut.dao.TutorRepository;
import com.vivraut.entity.Autism;
import com.vivraut.entity.Tutor;

/**
 * Servlet implementation class InscriptionAutiste
 */
//@WebServlet( name="InscriptionAutiste", urlPatterns = "/inscriptionAutiste" )
@Controller
public class InscriptionAutiste {
	
	public static final String VUE = "/listeAutistes";
	public static final String VUE_ECHEC = "/saisieAutiste";
	public  int ID_AUTISM;
	public  int ID_SCHEDULE;
	public  int ID_TUTOR;
    public static final String ATT_ERREURS  = "erreurs";
    public static final String ATT_RESULTAT = "resultat";
    public static final String CHAMP_TUTOR   = "tutor";
    
    
    String FIRSTNAME_AUTISM = null;
    String LASTNAME_AUTISM = null;
    String LOGIN_AUTISM = null;
    String PASSWORD_AUTISM = null;
    String CONFIRMATION_PASSWORD_AUTISM = null;
    String MAIL_AUTISM = null;        
    String IMEI_AUTISM = null;
    String PHONE_NUMBER_AUTISM = null;     

    @Autowired
    AutismRepository autRepo;
    
    @Autowired
    TutorRepository tutRepo;
    
    @GetMapping("/inscriptionAutiste")
    public String doGet() {
        /* Affichage de la page d'inscription */
        return VUE;
    }
    
    @PostMapping("/inscriptionAutiste")
    public String doPost(HttpServletRequest request, HttpSession session) {
        String resultat;
        Map<String, String> erreurs = new HashMap<String, String>();
    	
    	/* Récupération des champs du formulaire. */
        FIRSTNAME_AUTISM = request.getParameter("f_FIRSTNAME_AUTISM");
        LASTNAME_AUTISM = request.getParameter("f_LASTNAME_AUTISM");
        LOGIN_AUTISM = request.getParameter( "f_LOGIN_AUTISM");
        PASSWORD_AUTISM = request.getParameter( "f_PASSWORD_AUTISM");
        CONFIRMATION_PASSWORD_AUTISM = request.getParameter( "f_CONFIRMATION_PASSWORD_AUTISM");
        MAIL_AUTISM = request.getParameter( "f_MAIL_AUTISM");        
        IMEI_AUTISM = request.getParameter( "f_IMEI_AUTISM");
        PHONE_NUMBER_AUTISM = request.getParameter( "f_PHONE_NUMBER_AUTISM");     

        
       
        
        /* Validation du champ nom. */
        try {
            validationNom( LASTNAME_AUTISM );
        } catch ( Exception e ) {
            erreurs.put( "LASTNAME_AUTISM", e.getMessage() );
        }
        
        /* Validation du champ tel. */
        try {
            validationTel( PHONE_NUMBER_AUTISM );
        } catch ( Exception e ) {
            erreurs.put( "PHONE_NUMBER_AUTISM", e.getMessage() );
        }
        
        /* Validation des champs mot de passe et confirmation. */
        try {
            validationMotsDePasse( PASSWORD_AUTISM, CONFIRMATION_PASSWORD_AUTISM );
        } catch ( Exception e ) {
            erreurs.put( "PASSWORD_AUTISM", e.getMessage() );
        }
        
        /* Validation du mail. */
        try {
            validationMail( MAIL_AUTISM );
        } catch ( Exception e ) {
            erreurs.put( "MAIL_AUTISM", e.getMessage() );
        }

    
    /* Initialisation du résultat global de la validation. */
    if ( erreurs.isEmpty() ) {
    	//ici procédure envoi dans la base
    	enregistrerAutiste(session);
        resultat = "Succès de l'inscription.";
    } else {
        resultat = "échec de l'inscription.";
    }

    /* Stockage du résultat et des messages d'erreur dans l'objet request */
    request.setAttribute( ATT_ERREURS, erreurs );
    request.setAttribute( ATT_RESULTAT, resultat );

    /* Transmission de la paire d'objets request/response à notre JSP */
    if ( erreurs.isEmpty() ) {
    	Tutor t = (Tutor) session.getAttribute(CHAMP_TUTOR);	
		List<Autism> lAu = autRepo.findByTutor(t);
		request.setAttribute("autistes", lAu);
    	return VUE;
    }
    return VUE_ECHEC;
    }
  
    
    /**
     * Valide le nom d'autiste saisi.
     */
    private void validationNom( String LASTNAME_AUTISM ) throws Exception {
        if ( LASTNAME_AUTISM != null && LASTNAME_AUTISM.trim().length() < 3 ) {
            System.out.println("Le nom d'utilisateur doit contenir au moins 3 caractères : " + LASTNAME_AUTISM);
            throw new Exception( "Le nom d'utilisateur doit contenir au moins 3 caractères." );
        }
    }

    /**
     * Valide du tel de l'autiste saisi
     */

    private void validationTel( String PHONE_NUMBER_AUTISM ) throws Exception {
        if ( PHONE_NUMBER_AUTISM == null ) {
            throw new Exception( "La saisie du numéro de téléphone est obligatoire." );
        }      
    }
           
    
        /**
         * Valide les mots de passe saisis.
         */
        private void validationMotsDePasse( String PASSWORD_AUTISM, String CONFIRMATION_PASSWORD_AUTISM ) throws Exception{
            if (PASSWORD_AUTISM != null && PASSWORD_AUTISM.trim().length() != 0 && CONFIRMATION_PASSWORD_AUTISM != null && CONFIRMATION_PASSWORD_AUTISM.trim().length() != 0) {
                if (!PASSWORD_AUTISM.equals(CONFIRMATION_PASSWORD_AUTISM)) {
                    throw new Exception("Les mots de passe entrés sont différents, merci de les saisir à nouveau.");
                } 
                else if (PASSWORD_AUTISM.trim().length() < 3) {
                    throw new Exception("Les mots de passe doivent contenir au moins 3 caractères.");
                }
            } else {
                throw new Exception("Merci de saisir et confirmer votre mot de passe.");
            }
        
        }
        private void validationMail( String MAIL_AUTISM ) throws Exception {
            if ( MAIL_AUTISM != null && MAIL_AUTISM.trim().length() != 0 ) {
                if ( !MAIL_AUTISM.matches( "([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)" ) ) {
                    throw new Exception( "Merci de saisir une adresse mail valide." );
                }
            } else {
                throw new Exception( "Merci de saisir une adresse mail." );
            }
        }
        
        private void enregistrerAutiste(HttpSession session){
        	Tutor tu = (Tutor)session.getAttribute(CHAMP_TUTOR);
        	Autism autist=new Autism(
        			FIRSTNAME_AUTISM, 
        			BigInteger.valueOf(Long.parseLong(IMEI_AUTISM)), 
        			LASTNAME_AUTISM, 
        			LOGIN_AUTISM, 
        			MAIL_AUTISM,
        			PASSWORD_AUTISM, 
        			PHONE_NUMBER_AUTISM, 
        			tu);
        	autRepo.save(autist);
        }
    }