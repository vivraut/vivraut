package com.vivraut.servlet;

import java.math.BigDecimal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.vivraut.dao.*;
import com.vivraut.entity.*;

@Controller
public class ListeActivites {
	public static final String CHAMP_TUTOR = "tutor";
	public static final String VUE = "/listeActivites";
	public static final String VUE_AJOUT = "/ajoutActivite";
	public static final String VUE_MODIF = "/updateActivite";
	public static final String VUE_MODIF2 = "/updateActivite2";
	public static final String VUE_SUPPR = "/supprimeActivite";

	public static final String CHAMP_AUTISTE = "autism";
	public static final String CHAMP_ACTIVITE = "activite";
	public static final String CHAMP_LOCATION = "location";
  
	@Autowired
	AutismRepository autRepo;
	
	@Autowired
	ActivityRepository actRepo;
		
	@GetMapping("/listeActivites")
	protected String doGet(HttpServletRequest request, HttpSession session) {
		Tutor t = (Tutor)session.getAttribute(CHAMP_TUTOR);
		List<Autism> au = autRepo.findByTutor(t);
		System.out.println(au.get(0).getFirstnameAutism());
		request.setAttribute("autiste", au);
		request.setAttribute("cache", true); //permet de cacher la liste d'activités avant de choisir un autiste
		return VUE;
	}

	@PostMapping("/listeActivites")
	protected String doPost(HttpServletRequest request, HttpSession session) {
		 /* Transmission de la paire d'objets request/response à notre JSP */
		
		String action = request.getParameter("actionActivite");
		String actionSuppr = request.getParameter("actionActivite2");
		Tutor t = (Tutor)session.getAttribute(CHAMP_TUTOR);
		List<Autism> aulist = autRepo.findByTutor(t);
		System.out.println(aulist.get(0).getFirstnameAutism());
		request.setAttribute("autiste", aulist);
		request.setAttribute("cache", false);
		//Integer idAutiste = Integer.parseInt(request.getParameter("choixAutiste"));
		
		if(action!=null) {
			if(action.equals("Valider")){	
				Integer idAutiste = Integer.parseInt(request.getParameter("choixAutiste"));
				Autism au = autRepo.findOne(idAutiste);
				List<Activity> a = au.getActivities();
				request.setAttribute("activity", a);
				session.setAttribute(CHAMP_AUTISTE, au);
				System.out.println(au.getIdAutism());
				return VUE;
			}
			else if(action.equals("Suppression")){
				Integer idAutiste = Integer.parseInt(request.getParameter("choixAutiste"));
				//Integer idActivite = Integer.parseInt(request.getParameter("choixActivite"));
				//System.out.println("Supression de l'activit� avec l'ID : " + idActivite);
				//actRepo.delete(idActivite);
				Autism au = autRepo.findOne(idAutiste);
				List<Activity> a = au.getActivities();
				request.setAttribute("activity", a);
				return VUE_SUPPR;
			}
			else if(action.equals("Modifier")){
				Integer idActivite = Integer.parseInt(request.getParameter("choixActivite"));
				Activity act = actRepo.findOne(idActivite);
				session.setAttribute(CHAMP_ACTIVITE, act);
				session.setAttribute(CHAMP_LOCATION, act.getLocation());
				request.setAttribute("activite", act);
				request.setAttribute("location", act.getLocation());
				if(act.getLocation()!=null) {
					request.setAttribute("nullLongitude", act.getLocation().getGitudeLocation().compareTo(BigDecimal.ZERO)==0?"true":"false");
					request.setAttribute("nullLatitude", act.getLocation().getLatitudeLocation().compareTo(BigDecimal.ZERO)==0?"true":"false");
					return VUE_MODIF;
				}
				else
					return VUE_MODIF2;
			}
			else if(action.equals("Ajout")) {
				return VUE_AJOUT;
			}
		}
		if(actionSuppr!=null) {
			if(actionSuppr.equals("Confirmer")){
				Integer idAutiste = Integer.parseInt(request.getParameter("choixActiviteSuppr"));
				System.out.println("Suppression idAutiste " + idAutiste);
				actRepo.delete(idAutiste);
				request.setAttribute("cache", true);
			}
			else if(actionSuppr.equals("Annuler")){
				request.setAttribute("cache", true);
			}
		}
	    return VUE;
	}

}
