package com.vivraut.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * Servlet implementation class SuppressionAutiste
 */
@WebServlet("/gestionAutiste/SuppressionAutiste")
@Controller
public class SuppressionAutiste {
	
	@GetMapping("/gestionAutiste/SuppressionAutiste")
   	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	@PostMapping("/gestionAutiste/SuppressionAutiste")
   	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
