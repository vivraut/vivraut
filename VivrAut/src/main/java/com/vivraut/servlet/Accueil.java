package com.vivraut.servlet;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.ApplicationScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.vivraut.dao.TutorRepository;
import com.vivraut.entity.Tutor;

@Controller
//@ApplicationScope
public class Accueil {
	public static final String VUE_ACCUEIL = "/accueil";
	public static final String VUE_INSCRIPTION = "/inscription";
	public static final String VUE_SUCCES = "/succesConnexion";
	public static final String VUE_OUBLI = "/reinitMotdepasse";
	public static final String CHAMP_EMAIL = "email";
	public static final String CHAMP_MOTDEPASSE = "motdepasse";
	public static final String CHAMP_TUTOR = "tutor";
	public static final String CHAMP_ERREUR = "erreur";

	boolean bsession = false;
	
	@Autowired
	TutorRepository tutorDAO;

	@RequestMapping (value = "/resetsession" , method = RequestMethod.GET)
	public  @ResponseBody Boolean resetSession () {
		bsession=false;
		return bsession;
	}	
	
	/*@RequestMapping (value = "/setsession" , method = RequestMethod.PUT)
	public @ResponseBody Boolean setsession() {
		System.out.println ("Accueil session = " + bsession);
		bsession = true;
		return bsession;
	}*/

	@RequestMapping (value = "/getsession" , method = RequestMethod.GET)
	public @ResponseBody Boolean getsession() {
		System.out.println ("Accueil session = " + bsession);
		return bsession;
	}
	
	@RequestMapping(value = "/accueil" , method = RequestMethod.GET)
	protected String doGet()
	{
		return VUE_ACCUEIL ;
	}

	@RequestMapping(value = "/accueil" , method = RequestMethod.POST)
	protected String doPost(@RequestParam String actionAccueil, @RequestParam String email,
			@RequestParam String motdepasse, Map<String, Object> model, HttpSession session, HttpServletRequest request) {
		
		
		if (actionAccueil.equals("Inscription"))
			return VUE_INSCRIPTION;
		else if (actionAccueil.equals("Connexion")) {
			String erreur = "L'identifiant et/ou le mot de passe sont invalides !";

			Tutor t = tutorDAO.findOneByMailTutorAndPasswordTutor(email, motdepasse);

			if (t != null) {
				session.setAttribute(CHAMP_TUTOR, t);
				bsession = true;
				return VUE_SUCCES;
			} else {
				model.put(CHAMP_ERREUR, erreur);
			}
		}
		else if (actionAccueil.equals("Oubli mot de passe")) {
			request.setAttribute("cache", true);
			return VUE_OUBLI;
		}
		return VUE_ACCUEIL;
	}

	/*
	@GetMapping("/")
	String uid() {
		UUID uid = (UUID) session.getAttribute("uid");
		if (uid == null) {
			uid = UUID.randomUUID();
		}
		session.setAttribute("uid", uid); return uid.toString();
	}*/
}
