<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/bootstrap-responsive.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Droid+Sans:400,700">
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Droid+Serif">
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Boogaloo">
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Economica:700,400italic">
<script src="js/jquery-1.8.2.js"></script>


<title>Liste des activités de la personne</title>
</head>
<body>
<div class="container">
<div id="contact-form">
<form method="post" action="<c:url value="/listeActivites"/>">
<div class="span4">
	<div class="title"><h3>Sélectionner la personne dont vous voulez gérer les activités</h3></div>
	<select name="choixAutiste">
		<c:forEach items="${autiste}" var="aut" varStatus="status">
			<option value="${aut.idAutism}"><c:out value="${aut.firstnameAutism}"/></option>	
		</c:forEach>
	</select>
	<input type="submit" name="actionActivite" value="Valider" class="btn btn-succes btn-large"/><br/><br/>
	<c:if test="${cache==false}">
	<div class="title"><h3>Liste des activités</h3></div>
	<select name="choixActivite">
		<c:forEach items="${activity}" var="act" varStatus="status">
			<option value="${act.idActivity}"><c:out value="${act.nameActivity}"/></option>	
		</c:forEach>
	</select>
	<input type="submit" name="actionActivite" value="Suppression" class="btn btn-succes btn-large"/>
	<input type="submit" name="actionActivite" value="Modifier" class="btn btn-succes btn-large"/>
	<input type="submit" name="actionActivite" value="Ajout" class="btn btn-succes btn-large"/>
	</c:if>
	</div>
</form>
</div>
</div>
			<!-- start: Hero Unit - Main hero unit for a primary marketing message or call to action BAS DE PAGE-->
			<div class="container">
			<div class="hero-unit">
				<p>L'application qui vous permet d'organiser l'agenda d'un enfant autiste et de suivre ses activités.</p>
				<p>La vie est belle !</p>
				<p>
					<a class="btn btn-success btn-large" href="" onclick='history.back()'>Retour &raquo;</a>
				</p>
			</div>
			</div>
			<!-- end: Hero Unit -->
</body>
</html>