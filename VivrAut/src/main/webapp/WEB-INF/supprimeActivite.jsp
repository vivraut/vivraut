<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE>
<html>
<head>
<meta charset="UTF-8">
<title>Gestion des activités</title>
<%--  On ramene en une fois tous les paramètres du header --%>

    <!-- start: CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Droid+Sans:400,700">
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Droid+Serif">
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Boogaloo">
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Economica:700,400italic">
	<!-- end: CSS -->

</head>


<body>
<div class="container">
<div id="contact-form">
<form id="form1" method="post" action="listeActivites">

<fieldset id="fs1">

	<div class="span4">
	
	 					
	<div class="title"><h3>Attention !</h3></div>

	
<form method="post" action="<c:url value="/listeActivites"/>">
<br>
<h3>La suppression de l'activité détruira toutes les données qui la concerne</h3>
<br /><br>
	<h4>Sélectionner l'activité</h4>
<br /><br>	
	<select name="choixActiviteSuppr">
		<c:forEach items="${activity}" var="act">
			<option value="${act.idActivity}"><c:out value="${act.nameActivity} "/></option>	
		</c:forEach>
	</select>
<br /><br>	
	<input type="submit" name="actionActivite2" value="Confirmer" class="btn btn-succes btn-large"/>
	<input type="submit" name="actionActivite2" value="Annuler" class="btn btn-succes btn-large"/><br/><br/>


</div>

					<a class="brand" href="#"><img src="img/parallax-slider/twitter.png"
						alt="Logo"></a>

</fieldset>
</form>
</div>
</div>
</body>

</html>