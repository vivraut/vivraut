<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE>
<html>
<head>
<meta charset="UTF-8">
<title>Gestion des autistes</title>
<%--  On ramene en une fois tous les paramètres du header --%>

    <!-- start: CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Droid+Sans:400,700">
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Droid+Serif">
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Boogaloo">
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Economica:700,400italic">
	<!-- end: CSS -->

</head>


<body>
<div class="container">
<div id="contact-form">
<form id="form1" method="post" action="inscriptionAutiste">

<fieldset id="fs1">

	<div class="span4">
	<div class="title"><h3>Saisie d'un autiste</h3></div>

	
	<label for="FIRSTNAME_AUTISM">Prénom de l'autiste</label>

    <input type="text" id="f_FIRSTNAME_AUTISM" name="f_FIRSTNAME_AUTISM" size="20" maxlength="50" />

    <br />
    
	<label for="LASTNAME_AUTISM">Nom de l'autiste <span class="requis">*</span></label>
    <input type="text" id="f_LASTNAME_AUTISM" name="f_LASTNAME_AUTISM" size="20" maxlength="100" />
    <span class="erreur">${erreurs['LASTNAME_AUTISM']}</span>
	<br />
	
	<label for="LOGIN_AUTISM">Login de l'autiste</label>
    <input type="text" id="f_LOGIN_AUTISM" name="f_LOGIN_AUTISM" size="20" maxlength="50" />
    <br />
    
    <label for="PASSWORD_AUTISM">Mot de passe<span class="requis">*</span></label>
    <input type="password" id="f_PASSWORD_AUTISM" name="f_PASSWORD_AUTISM" size="20" maxlength="100" />
    <span class="erreur">${erreurs['PASSWORD_AUTISM']}</span>
	<br />
	
    <label for="CONFIRMATION_PASSWORD_AUTISM">Confirmez le mot de passe<span class="requis">*</span></label>
    <input type="password" id="f_CONFIRMATION_PASSWORD_AUTISM" name="f_CONFIRMATION_PASSWORD_AUTISM" size="20" maxlength="100" />
	<span class="erreur">${erreurs['CONFIRMATION_PASSWORD_AUTISM']}</span>
	<br />

	<label for="MAIL_AUTISM">Email de l'autiste<span class="requis">*</span></label>
    <input type="text" id="f_MAIL_AUTISM" name="f_MAIL_AUTISM" size="80" maxlength="200"/>
    <span class="erreur">${erreurs['MAIL_AUTISM']}</span>
   
     
	<br />
	
	<label for="IMEI_AUTISM">IMEI du téléphone</label>
    <input type="text" id="f_IMEI_AUTISM" name="f_IMEI_AUTISM" size="20" maxlength="20" />
	<br />
	
	<label for="COUNTRY_INDICATOR_AUTISM">Indicatif pays et n° de téléphone <span class="requis">*</span> </label>
    <input type="text" id="f_PHONE_NUMBER_AUTISM" name="f_PHONE_NUMBER_AUTISM" size="20" maxlength="15"
    pattern="^[0-9]*$"/>
     <span class="erreur">${erreurs['PHONE_NUMBER_AUTISM']}</span>
	<br /><br />

	<br /><br>

	<input type="submit" name="action" value="Valider la saisie" class="btn btn-succes btn-large"/><br /><br />  

</div>
</fieldset>
</form>
</div>
</div>
			<!-- start: Hero Unit - Main hero unit for a primary marketing message or call to action BAS DE PAGE-->
			<div class="container">
			<div class="hero-unit">
				<p>L'application qui vous permet d'organiser l'agenda d'un enfant autiste et de suivre ses activités.</p>
				<p>La vie est belle !</p>
				<p>
					<a class="btn btn-success btn-large" href="" onclick='history.back()'>Retour &raquo;</a>
				</p>
			</div>
			</div>
			<!-- end: Hero Unit -->

</body>

</html>