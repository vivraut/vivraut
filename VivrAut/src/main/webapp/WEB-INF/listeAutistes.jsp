<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%-- Définition d'une variable 
<%
String lenom = "Antoine";
%>

<%--
	//String prenom = request.getAttribute("prenom").toString();
--%>
<%-- Comme on utilise useBean cela permet de ne pas utiliser de Java dans une page JSP 
  on peut donc mettre en commentaire String prenom qui est du Java
<jsp:useBean id="prenom" scope="request" class="java.lang.String"></jsp:useBean>
--%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/bootstrap-responsive.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="http://fonts.googleapis.com/css?family=Droid+Sans:400,700">
<link rel="stylesheet" type="text/css"
	href="http://fonts.googleapis.com/css?family=Droid+Serif">
<link rel="stylesheet" type="text/css"
	href="http://fonts.googleapis.com/css?family=Boogaloo">
<link rel="stylesheet" type="text/css"
	href="http://fonts.googleapis.com/css?family=Economica:700,400italic">
<%--  On ramene en une fois tous les param�tres du header --%>

    <!-- start: CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Droid+Sans:400,700">
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Droid+Serif">
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Boogaloo">
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Economica:700,400italic">
	<!-- end: CSS -->	
	
	
<title>Liste des autistes</title>
</head>

<script src="js/jquery-1.8.2.js"></script>

<body>

<div class="container">
<div id="contact-form">


	<form method="post" action="<c:url value="/listeAutistes"/>">
	<fieldset id="fs1">
	
	<div class="title"><h2 >Liste des personnes à suivre</h2></div>
	<br>
	
		<div class="table"><table>
		<thead>
			<td>Prénom</td>
			<td>Nom</td>
			<td>Tel</td>
			<td>Mail</td>
			<td>Id</td>
			<td>Action</td>
		</thead>
			<c:forEach var="aut" items="${autistes}">
				<tr>
					<td><c:out value="${aut.firstnameAutism}"></c:out></td>
					<td><c:out value="${aut.lastnameAutism}"></c:out></td>
					<td><c:out value="${aut.phoneNumberAutism}"></c:out></td>
					<td><c:out value="${aut.mailAutism}"></c:out></td>
					<td><c:out value="${aut.idAutism}"></c:out></td>
	            
					<%-- <td><a href="listeAutistes?action=update&id=${aut.idAutism}">
							<img src="img/edit.png" />
					</a> <a href="listeAutistes?action=delete&id=${aut.idAutism}"> <img
							src="img/delete.png" />
					</a></td> --%>

				</tr>
			</c:forEach>
		</table>

		</div>
		<input type="submit" name="actionAutiste" value="Ajout" class="btn btn-succes btn-large"/> 
		<input type="submit" name="actionAutiste" value="Suppression" class="btn btn-succes btn-large"/>		
		
	</fieldset>
	</form>

</div>
</div>

			<!-- start: Hero Unit - Main hero unit for a primary marketing message or call to action BAS DE PAGE-->
			<div class="container">
			<div class="hero-unit">
				<p>L'application qui vous permet d'organiser l'agenda d'un enfant autiste et de suivre ses activités.</p>
				<p>La vie est belle !</p>
				<p>
					<a class="btn btn-success btn-large" href="" onclick='history.back()'>Retour &raquo;</a>
				</p>
			</div>
			</div>
			<!-- end: Hero Unit -->
</body>

</html>