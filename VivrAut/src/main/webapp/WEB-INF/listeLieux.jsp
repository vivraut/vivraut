<%@ page language="java" contentType="text/html; charset=UTF-8"
	session="true" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>



<%--============================================ 
CETTE PAGE EST SENSEE ETRE CONCUE SUR LA MEME MAQUETTE QUE LISTE AUTISTES
ELLE APPELLE LA SAISIE OU LA SUPPRESSION D'UN LIEU
KIKI PEUT LA TERMINER ????

LE PASSAGE SOUS SPRING A ETE FATAL....
================================================--%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/bootstrap-responsive.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="http://fonts.googleapis.com/css?family=Droid+Sans:400,700">
<link rel="stylesheet" type="text/css"
	href="http://fonts.googleapis.com/css?family=Droid+Serif">
<link rel="stylesheet" type="text/css"
	href="http://fonts.googleapis.com/css?family=Boogaloo">
<link rel="stylesheet" type="text/css"
	href="http://fonts.googleapis.com/css?family=Economica:700,400italic">
<%--  On ramène en une fois tous les paramètres du header --%>

    <!-- start: CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Droid+Sans:400,700">
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Droid+Serif">
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Boogaloo">
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Economica:700,400italic">
	<!-- end: CSS -->	
	
	
<title>Liste des lieux</title>
</head>

<script src="js/jquery-1.8.2.js"></script>

<body>

<div class="container">
<div id="contact-form">


	<form method="post" action="<c:url value="/listeLieux"/>">
	<fieldset id="fs1">
	
	<div class="title"><h2 >Liste des lieux</h2></div>
	<br>
	
		<div class="table"><table>
		<thead>
			<td>Id</td>
			<td>Nom</td>
			<td>Adresse</td>
			<td>CP</td>
			<td>Ville</td>
			<td>Longitude</td>
			<td>Latitude</td>
		</thead>
		
		
<%--============================================ 
POUR COMMENTAIRES
================================================--%>
		
		
			<c:forEach var="lieu" items="${lieux}">
				<tr>
					<td><c:out value="${lieu.idLocation}"></c:out></td>	
					<td><c:out value="${lieu.nameLocation}"></c:out></td>
					<td><c:out value="${lieu.addressLocation}"></c:out></td>
					<td><c:out value="${lieu.postalCodeLocation}"></c:out></td>
					<td><c:out value="${lieu.cityLocation}"></c:out></td>	
					<td><c:out value="${lieu.gitudeLocation}"></c:out></td>	
					<td><c:out value="${lieu.latitudeLocation}"></c:out></td>	
																			

	            
<%--============================================ 
Le bouton Ajout est sensé pointer sur saisieLieu
LE bouton supprimer est sensé pointer sur supprimerLieu (qui n'existe pas)
================================================--%>

				</tr>
			</c:forEach>
		</table>

		</div>
		<input type="submit" name="actionLieux" value="Ajout" class="btn btn-succes btn-large"/> 
		<input type="submit" name="actionLieux" value="Suppression" class="btn btn-succes btn-large"/>		
		
	</fieldset>
	</form>

</div>
</div>

			<!-- start: Hero Unit - Main hero unit for a primary marketing message or call to action BAS DE PAGE-->
			<div class="container">
			<div class="hero-unit">
				<p>L'application qui vous permet d'organiser l'agenda d'un enfant autiste et de suivre ses activités.</p>
				<p>La vie est belle !</p>
				<p>
					<a class="btn btn-success btn-large" href="" onclick='history.back()'>Retour &raquo;</a>
				</p>
			</div>
			</div>
			<!-- end: Hero Unit -->
</body>

</html>