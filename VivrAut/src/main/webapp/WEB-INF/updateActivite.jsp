<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/bootstrap-responsive.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="http://fonts.googleapis.com/css?family=Droid+Sans:400,700">
<link rel="stylesheet" type="text/css"
	href="http://fonts.googleapis.com/css?family=Droid+Serif">
<link rel="stylesheet" type="text/css"
	href="http://fonts.googleapis.com/css?family=Boogaloo">
<link rel="stylesheet" type="text/css"
	href="http://fonts.googleapis.com/css?family=Economica:700,400italic">
<title>Modification activité</title>
</head>
<body>
<div class="container">
<div id="contact-form">
	<form method="post" enctype="multipart/form-data" action="<c:url value="/updateActivite"/>">
		<fieldset>
			<div class="span4">
			<div class="title"><h3>Modification d'une activité</h3></div>
			<label for="nom">Nom de l'activité <span class="requis">*</span></label>
			<input type="text" id="nom" name="nom"
				value="${activite.nameActivity}" size="20" maxlength="60" />
			<span class="erreur">${va.erreurs['nom']}</span><br/>
			
			<label for="url">Image (Taille inférieure à 5 Mo)</label>
			<input type="file" id="url" name="url" accept="image/*" size="20" />
			<span class="erreur">${va.erreurs['url']}</span><br/><br/>
			
			<label for="duree">Durée de l'activité </label>
			<input type="text" id="duree" name="duree"
				value="<c:out value="${activite.defaultSecondDurationActivity}"/>" size="20" maxlength="60" />
			<span class="erreur">${va.erreurs['duree']}</span><br/><br/>
			
			<label for="nomLieu">Nom du lieu</label>
			<input type="text" id="nomLieu" name="nomLieu" placeholder="Nom du lieu"
				value="<c:out value="${location.nameLocation}"/>" size="20" maxlength="60" />
			<span class="erreur">${va.erreurs['nomLieu']}</span><br/>
			
			<label for="adresse">Adresse</label>
			<input type="text" id="adresse" name="adresse" 
				value="<c:out value="${location.addressLocation}"/>" size="20" maxlength="100" />
			<span class="erreur">${va.erreurs['adresse']}</span><br/>
			
			<label for="codePostal">Code postal</label>
			<input type="text" id="codePostal" name="codePostal"
				value="<c:out value="${location.postalCodeLocation}"/>" size="10" maxlength="10" />
			<span class="erreur">${va.erreurs['codePostal']}</span><br/>
			
			<label for="ville">Ville</label>
			<input type="text" id="ville" name="ville"
				value="<c:out value="${location.cityLocation}"/>" size="20" maxlength="60" />
			<span class="erreur">${va.erreurs['ville']}</span><br/>
			
			<label for="longitude">Longitude (mettre 0 si non rensigné)</label>
			<c:if test="${nullLongitude==false }">
			<input type="text" id="longitude" name="longitude"
				value="<c:out value="${location.gitudeLocation}"/>" size="20" maxlength="60" />
			<span class="erreur">${va.erreurs['longitude']}</span><br/>
			</c:if>
			<c:if test="${nullLongitude==true }">
			<input type="text" id="longitude" name="longitude"
				value="<c:out value="0"/>" size="20" maxlength="60" />
			<span class="erreur">${va.erreurs['longitude']}</span><br/>
			</c:if>
			<label for="latitude">Latitude (mettre 0 si non rensigné)</label>
			<c:if test="${nullLatitude==false }">
			<input type="text" id="latitude" name="latitude"
				value="<c:out value="${location.latitudeLocation}"/>" size="20" maxlength="60" />
			<span class="erreur">${va.erreurs['latitude']}</span><br/>
			</c:if>
			<c:if test="${nullLatitude==true }">
			<input type="text" id="latitude" name="latitude"
				value="<c:out value="0"/>" size="20" maxlength="60" />
			<span class="erreur">${va.erreurs['latitude']}</span><br/>
			</c:if>
			<input type="submit" name="actionAjouter" value="Ajouter" class="btn btn-succes btn-large" /> <br />
			</div>
		</fieldset>
	</form>
	</div>
	</div>
</body>
			<!-- start: Hero Unit - Main hero unit for a primary marketing message or call to action BAS DE PAGE-->
			<div class="container">
			<div class="hero-unit">
				<p>L'application qui vous permet d'organiser l'agenda d'un enfant autiste et de suivre ses activités.</p>
				<p>
					<a class="btn btn-success btn-large" href="" onclick='history.back()'>Retour &raquo;</a>
				</p>
			</div>
			</div>
			<!-- end: Hero Unit -->
</html>