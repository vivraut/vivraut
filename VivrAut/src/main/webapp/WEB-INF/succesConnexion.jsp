<%@page import="java.net.HttpRetryException"%>
<%@page import="com.vivraut.entity.Tutor"%>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="utf-8" />
	<title>Vivraut - Menu tuteur</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link type="text/css" rel="stylesheet" href="form.css" />
	<link rel="stylesheet" href="css/bootstrap.min.css">
  	<link rel="stylesheet" href="css/bootstrap-responsive.css">
  	<script src="js/bootstrap.js"></script>
</head>
<body>
	<p>Vous êtes bien connecté !</p>
	<span>Bienvenue ${sessionScope.tutor.firstnameTutor} ! Votre id est ${sessionScope.tutor.idTutor}.</span><br/><br/>
	<a href=<c:url value="/updateTuteur"/>><input type="button" value="Paramétrage Profil"/></a><br/>
	<a href=<c:url value="/inscriptionAutiste"/>><input type="button" value="Paramétrage Autiste"/></a><br/>
	<a href=<c:url value="/accueil"/>><input type="button" value="Planning"/></a><br/>
	<%
	  Tutor tu = (Tutor) request.getSession().getAttribute("tutor");
	  int Idt  = tu.getIdTutor();
	  response.sendRedirect("index.html?idt=" + Idt);
		
	%>
	<div class="list-group col-md-4">
		<a href="updateTuteur" class="list-group-item">Paramétrage Profil</a>
	    <a href="inscriptionAutiste" class="list-group-item">Paramétrage Autiste</a>
	    <a href="index.html" class="list-group-item">Planning</a>
    </div>
</body>
</html>