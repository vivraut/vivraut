<%@ page pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="UTF-8" />
	<title>Vivraut - Réinitialisation mot de passe</title>
   
    <!-- start: CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Droid+Sans:400,700">
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Droid+Serif">
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Boogaloo">
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Economica:700,400italic">
	<!-- end: CSS -->
</head>
<body>
<div class="container">
<div id="contact-form">

	<form method="post" action="oubliMotdePasse"/>
 	<!-- <form method="post" action=<c:url value="/accueil"/>>-->
		<fieldset>
			<div class="span4">
			<div class="title"><h3>Réinitialisation du mot de passe</h3></div>
		
			
			<p>Saisissez votre adresse mail et votre prénom.</p>
			<label for="email">Adresse email <span class="requis">*</span></label>
			<input type="text" id="email" name="email" placeholder="Adresse mail"
				value="${tutor.mailTutor}" size="20" maxlength="60" /><span class="erreur">${erreur['email']}</span> <br /> 
			<label for="prenom">Prénom <span class="requis">*</span></label> 
			<input type="text" id="prenom" name="prenom" value="${tutor.firstnameTutor}" size="20" maxlength="40" /> 
			<span class="erreur">${erreur['prenom']}</span><br />
			<input type="submit" name="actionOubli" value="Valider" class="btn btn-succes btn-large" class="sansLabel"/><br/><br/>
			
			<c:if test="${cache==false}">
				<label for="password">Nouveau mot de passe<span class="requis">*</span></label>
				<input type="password" id="password" name="password"
					value="" size="20" maxlength="60" /><span class="erreur"></span> <br /> 
				<label for="confPassword">Confirmation mot de passe <span class="requis">*</span></label> 
				<input type="password" id="confPassword" name="confPassword" value="" size="20" maxlength="40" /> <br />
				<input type="submit" name="actionOubli" value="Réinitialiser" class="btn btn-succes btn-large" class="sansLabel"/> 			
			</c:if>
			</div>
		</fieldset>
	</form>
</div>
</div>

<!-- start: Hero Unit - Main hero unit for a primary marketing message or call to action BAS DE PAGE-->
			<div class="container">
			<div class="hero-unit">
				<p>L'application qui vous permet d'organiser l'agenda d'un enfant autiste et de suivre ses activités.</p>
				<p>La vie est belle !</p>
				<p>
					<a class="btn btn-success btn-large" href="" onclick='history.back()'>Retour &raquo;</a>
				</p>
			</div>
			</div>
<!-- end: Hero Unit -->
</body>
</html>