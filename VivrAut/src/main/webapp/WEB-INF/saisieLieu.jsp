<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE>
<html>
<head>
<meta charset="UTF-8">
<title>Gestion des lieux d'activité</title>
<%--  On ramene en une fois tous les param�tres du header --%>

    <!-- start: CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Droid+Sans:400,700">
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Droid+Serif">
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Boogaloo">
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Economica:700,400italic">
	<!-- end: CSS -->

</head>


<body>

<div class="container">
<div id="contact-form">

<form id="form1" method="post" action="saisieLieu">

<fieldset>

	<div class="title"><h3>Saisie d'un lieu</h3></div>

	
	<label for="NAME_LOCATION">Nom du lieu<span class="requis">*</span></label>
    <input type="text" id="f_NAME_LOCATION" name="f_NAME_LOCATION" size="20" maxlength="50" />
    <span class="erreur">${erreurs['NAME_LOCATION']}</span>
    <br />
    
	<label for="ADDRESS_LOCATION">Adresse</label>
    <input type="text" id="f_ADDRESS_LOCATION" name="f_ADDRESS_LOCATION" size="20" maxlength="100" />
	<br />
	

	<label for="POSTAL_CODE_LOCATION">CP Lieu</label>
    <input type="text" id="f_POSTAL_CODE_LOCATION" name="f_POSTAL_CODE_LOCATION" size="20" maxlength="100" />
	<br />

	<label for="CITY_LOCATION">Ville</label>
    <input type="text" id="f_CITY_LOCATION" name="f_CITY_LOCATION" size="20" maxlength="100" />
	<br />
	
	<label for="GITUDE_LOCATION">Longitude</label>
    <input type="text" id="f_GITUDE_LOCATION" name="f_GITUDE_LOCATION" size="20" maxlength="100" value="0" />
	<br />
	
	<label for="LATITUDE_LOCATION">Latitude</label>
    <input type="text" id="f_LATITUDE_LOCATION" name="f_LATITUDE_LOCATION" size="20" maxlength="100" value="0"/>
	<br />
	
		<br /><br>

	<input type="submit" name="action" value="Ajout" class="btn btn-succes btn-large"/>

	<br /><br />  
</div>
</div>

</fieldset>
</form>


			<!-- start: Hero Unit - Main hero unit for a primary marketing message or call to action BAS DE PAGE-->
			<div class="container">
			<div class="hero-unit">
				<p>L'application qui vous permet d'organiser l'agenda d'un enfant autiste et de suivre ses activités.</p>
				<p>La vie est belle !</p>
				<p>
					<a class="btn btn-success btn-large" href="" onclick='history.back()'>Retour &raquo;</a>
				</p>
			</div>
			</div>
			<!-- end: Hero Unit -->

</body>
</html>