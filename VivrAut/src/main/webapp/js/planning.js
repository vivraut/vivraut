/**
 * 
 */

var app = angular.module("planningApp", []);
app
		.controller(
				"planningCtrl",
				function($scope, $http) {
					$scope.autismes = null;
					$scope.schedule = null;
					$scope.activity = null;
					$scope.date = Date.now();
					$scope.showLocation = false;
					$scope.editMode = 0;
					$scope.idAutismSelected = null;

					$scope.loadPlanning = function(idAutism) {
						var id = parseInt(idAutism);
						$scope.idAutismSelected = id;
						var reqSchedule = $http({
							method : 'GET',
							url : 'planning/' + id,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
							}
						/*
						 * , params: {startDate: new Date($scope.date)}
						 */
						// Erreur de parsing côté serveur
						});
						reqSchedule.then(function(data) {
							console.log(data);
							$scope.schedule = angular.fromJson(data.data);
							$scope.activity = null;
							$scope.showLocation = false;
							$scope.editMode = 0;
						}, function(error) {
							console.log("Erreur lors de l'envoi des données");
							console.log(error);
						});
					}

					$scope.loadDetails = function(idActivity) {
						var id = parseInt(idActivity);
						var reqDetails = $http({
							method : 'GET',
							url : 'activity/' + id,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
							}
						});
						reqDetails.then(function(data) {
							console.log(data);
							$scope.activity = angular.fromJson(data.data);
							$scope.showLocation = false;
						}, function(error) {
							console.log("Erreur lors de l'envoi des données");
							console.log(error);
						});
					}

					$scope.showLocationDetails = function() {
						$scope.showLocation = !$scope.showLocation;
					}

					$scope.editPlanningElement = function(idSchedule) {
						if ($scope.editMode == 0)
							$scope.editMode = idSchedule;
					}

					$scope.cancelPlanningElementEdition = function(idSchedule) {
						if (idSchedule == $scope.editMode)
							$scope.editMode = 0;
					}
					
					$scope.createPlanningElement = function() {
						
					}
					
					$scope.removePlanningElement = function(idSchedule) {
						var id = parseInt(idSchedule);
						$http.delete('planning/', {params: {idSchedule: id}})
							.then(function(data) {
								console.log(data);
								$scope.loadPlanning($scope.idAutismSelected);
							}, function(error) {
								console.log("Erreur lors de l'envoi des données");
								console.log(error);
							});
					}

					var reqAutistes = $http({
						method : 'GET',
						url : 'autistic',
						headers : {
							'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
						},
						data : ''
					});
					reqAutistes.then(function(data) {
						console.log(data);
						$scope.autismes = angular.fromJson(data.data);
						$scope.activity = null;
						$scope.showLocation = false;
						$scope.editMode = 0;
						$scope.idAutismSelected = null;
					}, function(error) {
						console.log("Erreur lors de l'envoi des données");
						console.log(error);
					});
				});