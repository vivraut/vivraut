package CompteAutiste;

import java.io.IOException;
import java.math.BigInteger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.ServiceAutismDAO;
import com.dao.ServiceTutorDAO;
import com.entities.Autism;
import com.entities.Tutor;

import java.util.HashMap;
import java.util.Map;

/**
 * Servlet implementation class InscriptionAutiste
 */

/*@WebServlet("/inscriptionAutiste")*/
@WebServlet( name="InscriptionAutiste", urlPatterns = "/inscriptionAutiste" )

public class InscriptionAutiste extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public static final String VUE = "/WEB-INF/saisieAutiste.jsp";
	public  int ID_AUTISM;
	public  int ID_SCHEDULE;
	public  int ID_TUTOR;
    public static final String ATT_ERREURS  = "erreurs";
    public static final String ATT_RESULTAT = "resultat";
    public static final String CHAMP_TUTOR   = "tutor";
    
    
    String FIRSTNAME_AUTISM = null;
    String LASTNAME_AUTISM = null;
    String LOGIN_AUTISM = null;
    String PASSWORD_AUTISM = null;
    String CONFIRMATION_PASSWORD_AUTISM = null;
    String MAIL_AUTISM = null;        
    String IMEI_AUTISM = null;
    String PHONE_NUMBER_AUTISM = null;     

        
    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
        /* Affichage de la page d'inscription */
        this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
    }
    
    public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
        String resultat;
        Map<String, String> erreurs = new HashMap<String, String>();
    	
    	/* R�cup�ration des champs du formulaire. */
        FIRSTNAME_AUTISM = request.getParameter("f_FIRSTNAME_AUTISM");
        LASTNAME_AUTISM = request.getParameter("f_LASTNAME_AUTISM");
        LOGIN_AUTISM = request.getParameter( "f_LOGIN_AUTISM");
        PASSWORD_AUTISM = request.getParameter( "f_PASSWORD_AUTISM");
        CONFIRMATION_PASSWORD_AUTISM = request.getParameter( "f_CONFIRMATION_PASSWORD_AUTISM");
        MAIL_AUTISM = request.getParameter( "f_MAIL_AUTISM");        
        IMEI_AUTISM = request.getParameter( "f_IMEI_AUTISM");
        PHONE_NUMBER_AUTISM = request.getParameter( "f_PHONE_NUMBER_AUTISM");     

        
       
        
        /* Validation du champ nom. */
        try {
            validationNom( LASTNAME_AUTISM );
        } catch ( Exception e ) {
            erreurs.put( "LASTNAME_AUTISM", e.getMessage() );
        }
        
        /* Validation du champ tel. */
        try {
            validationTel( PHONE_NUMBER_AUTISM );
        } catch ( Exception e ) {
            erreurs.put( "PHONE_NUMBER_AUTISM", e.getMessage() );
        }
        
        /* Validation des champs mot de passe et confirmation. */
        try {
            validationMotsDePasse( PASSWORD_AUTISM, CONFIRMATION_PASSWORD_AUTISM );
        } catch ( Exception e ) {
            erreurs.put( "PASSWORD_AUTISM", e.getMessage() );
        }
        
        /* Validation du mail. */
        try {
            validationMail( MAIL_AUTISM );
        } catch ( Exception e ) {
            erreurs.put( "MAIL_AUTISM", e.getMessage() );
        }

    
    /* Initialisation du r�sultat global de la validation. */
    if ( erreurs.isEmpty() ) {
    	//ici procedure envoi dans la base
    	enregistrerAutiste(request);
        resultat = "Succ�s de l'inscription.";
    } else {
        resultat = "�chec de l'inscription.";
    }

    /* Stockage du r�sultat et des messages d'erreur dans l'objet request */
    request.setAttribute( ATT_ERREURS, erreurs );
    request.setAttribute( ATT_RESULTAT, resultat );

    /* Transmission de la paire d'objets request/response � notre JSP */
    this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
    }
  
    
    /**
     * Valide le nom d'autiste saisi.
     */
    private void validationNom( String LASTNAME_AUTISM ) throws Exception {
        if ( LASTNAME_AUTISM != null && LASTNAME_AUTISM.trim().length() < 3 ) {
            System.out.println("Le nom d'utilisateur doit contenir au moins 3 caract�res : " + LASTNAME_AUTISM);
            throw new Exception( "Le nom d'utilisateur doit contenir au moins 3 caract�res." );
        }
    }

    /**
     * Valide du tel de l'autiste saisi
     */

    private void validationTel( String PHONE_NUMBER_AUTISM ) throws Exception {
        if ( PHONE_NUMBER_AUTISM == null ) {
            throw new Exception( "La saisie du num�ro de t�l�phone est obligatoire." );
        }      
    }
           
    
        /**
         * Valide les mots de passe saisis.
         */
        private void validationMotsDePasse( String PASSWORD_AUTISM, String CONFIRMATION_PASSWORD_AUTISM ) throws Exception{
            if (PASSWORD_AUTISM != null && PASSWORD_AUTISM.trim().length() != 0 && CONFIRMATION_PASSWORD_AUTISM != null && CONFIRMATION_PASSWORD_AUTISM.trim().length() != 0) {
                if (!PASSWORD_AUTISM.equals(CONFIRMATION_PASSWORD_AUTISM)) {
                    throw new Exception("Les mots de passe entr�s sont diff�rents, merci de les saisir � nouveau.");
                } 
                else if (PASSWORD_AUTISM.trim().length() < 3) {
                    throw new Exception("Les mots de passe doivent contenir au moins 3 caract�res.");
                }
            } else {
                throw new Exception("Merci de saisir et confirmer votre mot de passe.");
            }
        
        }
        private void validationMail( String MAIL_AUTISM ) throws Exception {
            if ( MAIL_AUTISM != null && MAIL_AUTISM.trim().length() != 0 ) {
                if ( !MAIL_AUTISM.matches( "([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)" ) ) {
                    throw new Exception( "Merci de saisir une adresse mail valide." );
                }
            } else {
                throw new Exception( "Merci de saisir une adresse mail." );
            }
        }
        
        private void enregistrerAutiste(HttpServletRequest request){
        	// On instancie les classes service DAO
        	com.dao.ServiceAutismDAO sadao = new ServiceAutismDAO();
        	com.dao.ServiceTutorDAO stdao = new ServiceTutorDAO();
        	
        	// On peut utiliser les m�thodes des classes service instanci�es
        	HttpSession session = request.getSession();
			Tutor tu = (Tutor)session.getAttribute(CHAMP_TUTOR);
        	Autism autist=new Autism(
        			FIRSTNAME_AUTISM, 
        			BigInteger.valueOf(Long.parseLong(IMEI_AUTISM)), 
        			LASTNAME_AUTISM, 
        			LOGIN_AUTISM, 
        			MAIL_AUTISM,
        			PASSWORD_AUTISM, 
        			PHONE_NUMBER_AUTISM, 
        			tu);
    		sadao.ajouter(autist);
			
        }
    }