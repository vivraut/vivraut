package com.servlets;

import java.io.Console;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.ServiceAutismDAO;
import com.entities.Activity;
import com.entities.Autism;
import com.entities.Tutor;

/**
 * Servlet implementation class ListeAutiste
 */
@WebServlet(name = "ListeAutistes", urlPatterns = "/listeAutistes")
public class ListeAutistes extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final String CHAMP_TUTOR = "tutor";
	public static final String VUE = "/WEB-INF/listeAutistes.jsp";
	public static final String VUE_AJOUT = "/WEB-INF/saisieAutiste.jsp";
	public static final String VUE_SUPP = "/WEB-INF/supprimeAutiste.jsp";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ListeAutistes() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 * response
	 */

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		String action = request.getParameter("actionAutiste");
		System.out.println(action);
		HttpSession session = request.getSession();
		Tutor t = (Tutor) session.getAttribute(CHAMP_TUTOR);
		
		ServiceAutismDAO saudao = new ServiceAutismDAO();
		HashMap<String, Object> filtre = new HashMap<>();
		filtre.put("tutor", t);
		List<Autism> lAu = new ArrayList<>();
		lAu = saudao.lister(filtre);
		request.setAttribute("autistes", lAu);
		String v = String.format("%d", lAu.size());
		System.out.println(v);
		this.getServletContext().getRequestDispatcher(VUE).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getParameter("actionAutiste");
		System.out.println(action);
		HttpSession session = request.getSession();
		Tutor t = (Tutor) session.getAttribute(CHAMP_TUTOR);
		System.out.println(t.getIdTutor());
		HashMap<String, Object> filtre = new HashMap<>();
		filtre.put("tutor", t);
		ServiceAutismDAO saudao = new ServiceAutismDAO();
		
		if (action.equals("Ajout")) {
			this.getServletContext().getRequestDispatcher(VUE_AJOUT).forward(request, response);
		}
		else if  (action.equals("Suppression")) {
			List<Autism> lAu = new ArrayList<>();
			lAu = saudao.lister(filtre);
			System.out.println("Supp Nb:" + lAu.size());
			request.setAttribute("autistes", lAu);
			this.getServletContext().getRequestDispatcher(VUE_SUPP).forward(request, response);

			}
		else if(action.equals("Confirmer")){
			int idAutiste = Integer.parseInt(request.getParameter("choixAutiste"));
			System.out.println("Suppression idAutiste " + idAutiste);
			saudao.supprimer(idAutiste);
			
		}

	}

}
