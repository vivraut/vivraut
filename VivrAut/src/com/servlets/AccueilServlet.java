package com.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.ServiceTutorDAO;
import com.entities.Tutor;


@SuppressWarnings("serial")
/*@WebServlet("/accueil")*/
@WebServlet( name="AccueilServlet", urlPatterns = "/accueil" )
public class AccueilServlet extends HttpServlet{
	public static final String VUE_ACCUEIL = "/WEB-INF/accueil.jsp";
    public static final String VUE_INSCRIPTION   = "/WEB-INF/inscription.jsp";
    public static final String VUE_SUCCES   = "/WEB-INF/succesConnexion.jsp";
    public static final String CHAMP_EMAIL   = "email";
    public static final String CHAMP_MOTDEPASSE   = "motdepasse";
    public static final String CHAMP_TUTOR   = "tutor";
    public static final String CHAMP_ERREUR   = "erreur";
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	
		getServletContext().getRequestDispatcher(VUE_ACCUEIL).forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("actionAccueil");
		if (action.equals("Inscription"))
			getServletContext().getRequestDispatcher(VUE_INSCRIPTION).forward(request, response);
		else if (action.equals("Connexion")){
			String email = request.getParameter(CHAMP_EMAIL);
			String motdepasse = request.getParameter(CHAMP_MOTDEPASSE);
			String erreur = "L'identifiant et/ou le mot de passe sont invalides !";
			ServiceTutorDAO stdao = new ServiceTutorDAO();
			Tutor t = stdao.trouverInscription(email, motdepasse);

			if(t != null){
				HttpSession session = request.getSession();
				session.setAttribute(CHAMP_TUTOR, t);
				getServletContext().getRequestDispatcher(VUE_SUCCES).forward(request, response);
			}
			else{
				request.setAttribute(CHAMP_ERREUR, erreur);
				getServletContext().getRequestDispatcher(VUE_ACCUEIL).forward(request, response);
			}
		}
	}
}
