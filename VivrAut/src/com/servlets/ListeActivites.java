package com.servlets;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.ServiceActivityDAO;
import com.dao.ServiceAutismDAO;
import com.dao.ServiceLocationDAO;
import com.dao.ServiceTutorDAO;
import com.entities.Activity;
import com.entities.Autism;
import com.entities.Tutor;

/**
 * Servlet implementation class ListeActivites
 */
@WebServlet(name = "ListeActivites" , urlPatterns = "/listeActivites")
public class ListeActivites extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final String CHAMP_TUTOR = "tutor";
	public static final String CHAMP_AUTISTE = "autism";
	public static final String CHAMP_ACTIVITE = "activite";
	public static final String CHAMP_LOCATION = "location";
	public static final String VUE = "/WEB-INF/listeActivites.jsp";
	public static final String VUE_AJOUT = "/WEB-INF/ajoutActivite.jsp";
	public static final String VUE_MODIF = "/WEB-INF/updateActivite.jsp";
	public static final String VUE_MODIF2 = "/WEB-INF/updateActivite2.jsp";
  
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListeActivites() {
        super();
        // TODO Auto-generated constructor stub
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Tutor t = (Tutor)session.getAttribute(CHAMP_TUTOR);
		ServiceAutismDAO saudao = new ServiceAutismDAO();
		List<Autism> au = new ArrayList<>();
		HashMap<String, Object> filtre = new HashMap<>();
		filtre.put("tutor", t);
		au = saudao.lister(filtre);
		//System.out.println(au.get(0).getFirstnameAutism());
		request.setAttribute("autiste", au);
		request.setAttribute("cache", true); //permet de cacher la liste d'activit�s avant de choisir un autiste
		this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		 /* Transmission de la paire d'objets request/response � notre JSP */
		
		String action = request.getParameter("actionActivite");
		HttpSession session = request.getSession();
		Tutor t = (Tutor)session.getAttribute(CHAMP_TUTOR);
		ServiceActivityDAO sadao = new ServiceActivityDAO();
		ServiceAutismDAO saudao = new ServiceAutismDAO();
		List<Autism> aulist = new ArrayList<>();
		HashMap<String, Object> filtre = new HashMap<>();
		filtre.put("tutor", t);
		aulist = saudao.lister(filtre);
		request.setAttribute("autiste", aulist);
		request.setAttribute("cache", false);
		//Integer idAutiste = Integer.parseInt(request.getParameter("choixAutiste"));
		
		if(action.equals("Valider")){	
			Integer idAutiste = Integer.parseInt(request.getParameter("choixAutiste"));
			Autism au = saudao.trouver(idAutiste);
			List<Activity> a = new ArrayList<>();
			HashMap<String, Object> filtre2 = new HashMap<>();
			filtre2.put("autism", au);
			a = sadao.lister(filtre2);
			request.setAttribute("activity", a);
			session.setAttribute(CHAMP_AUTISTE, au);
			System.out.println(au.getIdAutism());
			this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
		}
		else if(action.equals("Suppression")){
			Integer idAutiste = Integer.parseInt(request.getParameter("choixAutiste"));
			Integer idActivite = Integer.parseInt(request.getParameter("choixActivite"));
			Activity act = sadao.trouver(idActivite);
			ServiceLocationDAO sldao = new ServiceLocationDAO();
			if(act.getLocation()!=null)
				sldao.supprimer(act.getLocation().getIdLocation());
			//System.out.println(idActivite);		
			sadao.supprimer(idActivite);
			Autism au = saudao.trouver(idAutiste);
			List<Activity> a = new ArrayList<>();
			HashMap<String, Object> filtre2 = new HashMap<>();
			filtre2.put("autism", au);
			a = sadao.lister(filtre2);
			request.setAttribute("activity", a);
			this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
		}
		else if(action.equals("Modifier")){
			Integer idActivite = Integer.parseInt(request.getParameter("choixActivite"));
			Activity act = sadao.trouver(idActivite);
			//System.out.println("Location : " + act.getLocation().getIdLocation());
			session.setAttribute(CHAMP_ACTIVITE, act);
			session.setAttribute(CHAMP_LOCATION, act.getLocation());
			request.setAttribute("activite", act);
			request.setAttribute("location", act.getLocation());
			if(act.getLocation()!=null) {
			request.setAttribute("nullLongitude", act.getLocation().getGitudeLocation().compareTo(BigDecimal.ZERO)==0?"true":"false");
			request.setAttribute("nullLatitude", act.getLocation().getLatitudeLocation().compareTo(BigDecimal.ZERO)==0?"true":"false");
			this.getServletContext().getRequestDispatcher( VUE_MODIF ).forward( request, response );
			}
			else
				this.getServletContext().getRequestDispatcher( VUE_MODIF2 ).forward( request, response );
			//System.out.println("Location : " + (act.getLocation().getLogitudeLocation().toString()));
			//System.out.println("Location : " + (act.getLocation().getLogitudeLocation().compareTo(BigDecimal.ZERO)==0?"true":"false"));
		}
		else if(action.equals("Ajout")) {
			this.getServletContext().getRequestDispatcher( VUE_AJOUT ).forward( request, response );
		}
		
		//ServiceActivityDAO sadao = new ServiceActivityDAO();
		
		//System.out.println(au.get(0).getFirstnameAutism());
		/*List<Activity> a = new ArrayList<>();
		HashMap<String, Object> filtre2 = new HashMap<>();
		filtre2.put("autism", au.get(0));
		a = sadao.lister(filtre2);
		request.setAttribute("activity", a);*/
		//Activity a = sadao.trouver2(1);
		//System.out.println(a.get(0).getNameActivity());
	    //this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
	}

}
