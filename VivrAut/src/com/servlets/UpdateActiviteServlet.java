package com.servlets;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import com.dao.ServiceActivityDAO;
import com.dao.ServiceLocationDAO;
import com.entities.Activity;
import com.entities.Autism;
import com.entities.Location;
import com.formulaires.ValidationActivite;

@SuppressWarnings("serial")
@WebServlet("/updateActivite")
@MultipartConfig(
        fileSizeThreshold   = 1024 * 1024 * 5,  // 5 MB : limite taille fichier 
        maxFileSize         = 1024 * 1024 * 50, // 50 MB
        maxRequestSize      = 1024 * 1024 * 50, // 50 MB
        location            = "C:/"
)
public class UpdateActiviteServlet extends HttpServlet{
	
	public static final String ATT_ACTIVITE = "activite";
	public static final String ATT_LOCATION = "location";
	public static final String ATT_AUTISTE = "autism";
    public static final String ATT_FORM   = "va";
	public static final String VUE_SUCCES = "/index.html";
    public static final String VUE_FORM   = "/WEB-INF/updateActivite.jsp";
    public static final String CHEMIN        = "chemin";
    public static final int TAILLE_TAMPON = 10240; // 10 ko
    public static final String CHAMP_URL     = "url";
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		Autism aut = (Autism) session.getAttribute(ATT_AUTISTE);
		Activity activite = (Activity)session.getAttribute(ATT_ACTIVITE);
		Location location = (Location)session.getAttribute(ATT_LOCATION);
		//System.out.println(aut.getFirstnameAutism());
		//System.out.println("id loc : "+location.getIdLocation());
		ValidationActivite va = new ValidationActivite();
		//Location location = null;
		if(location!=null)
			location = va.creerLocation(request,location);
		else
			location = va.creerLocation(request);
		activite = va.creerActivite(request, aut, location,activite);
		//activite.setIdActivity((int) session.getAttribute("id_activite"));
		
		if(location!=null && location.getIdLocation()==0){
			ServiceLocationDAO sldao = new ServiceLocationDAO();
			location = sldao.ajouter(location);
			activite.setLocation(location);
			//System.out.println("id loc : "+location.getIdLocation());
		}
		request.setAttribute( ATT_LOCATION, location );
		request.setAttribute( ATT_ACTIVITE, activite );
        request.setAttribute( ATT_FORM, va );
        //System.out.println(activite.getIdActivity());
        if (va.getErreurs().isEmpty()){
        	//ServiceLocationDAO sldao = new ServiceLocationDAO();
        	//sldao.ajouter(location);
        	
        	//if (activite.getUrlPictureActivity()!=null && activite.getUrlPictureActivity()!="") {
        	//R�cup�ration de l'image
        	String chemin = this.getServletConfig().getServletContext().getRealPath("/")+"img\\app\\";
        	//System.out.println(chemin);
            /*
             * Les donn�es re�ues sont multipart, on doit donc utiliser la m�thode
             * getPart() pour traiter le champ d'envoi de fichiers.
             */
            Part part = request.getPart( CHAMP_URL );
                
            /*
             * Il faut d�terminer s'il s'agit d'un champ classique 
             * ou d'un champ de type fichier : on d�l�gue cette op�ration 
             * � la m�thode utilitaire getNomFichier().
             */
            String nomFichier = getNomFichier( part );

            /*
             * Si la m�thode a renvoy� quelque chose, il s'agit donc d'un champ
             * de type fichier (input type="file").
             */
            if ( nomFichier != null && !nomFichier.isEmpty() ) {
                String nomChamp = part.getName();
                /*
                 * Antibug pour Internet Explorer, qui transmet pour une raison
                 * mystique le chemin du fichier local � la machine du client...
                 * 
                 * Ex : C:/dossier/sous-dossier/fichier.ext
                 * 
                 * On doit donc faire en sorte de ne s�lectionner que le nom et
                 * l'extension du fichier, et de se d�barrasser du superflu.
                 */
                 nomFichier = nomFichier.substring( nomFichier.lastIndexOf( '/' ) + 1 )
                        .substring( nomFichier.lastIndexOf( '\\' ) + 1 );

                /* �criture du fichier sur le disque */
                ecrireFichier( part, nomFichier, chemin );
            }
            if(!nomFichier.equals(""))
            	activite.setUrlPictureActivity(nomFichier);
            ServiceActivityDAO sadao = new ServiceActivityDAO();
            if(location!=null)
            	sadao.update(activite,location);
            else
            	sadao.update(activite);
        	getServletContext().getRequestDispatcher(VUE_SUCCES).forward(request, response);
        	
        }
        else
        	getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
    }
    
    private static String getNomFichier( Part part ) {
        /* Boucle sur chacun des param�tres de l'en-t�te "content-disposition". */
        for ( String contentDisposition : part.getHeader( "content-disposition" ).split( ";" ) ) {
            /* Recherche de l'�ventuelle pr�sence du param�tre "filename". */
            if ( contentDisposition.trim().startsWith( "filename" ) ) {
                /*
                 * Si "filename" est pr�sent, alors renvoi de sa valeur,
                 * c'est-�-dire du nom de fichier sans guillemets.
                 */
                return contentDisposition.substring( contentDisposition.indexOf( '=' ) + 1 ).trim().replace( "\"", "" );
            }
        }
        /* Et pour terminer, si rien n'a �t� trouv�... */
        return null;
    }
    
    private void ecrireFichier( Part part, String nomFichier, String chemin ) throws IOException {
        /* Pr�pare les flux. */
        BufferedInputStream entree = null;
        BufferedOutputStream sortie = null;
        try {
            /* Ouvre les flux. */
            entree = new BufferedInputStream( part.getInputStream(), TAILLE_TAMPON );
            sortie = new BufferedOutputStream( new FileOutputStream( new File( chemin + nomFichier ) ),
                    TAILLE_TAMPON );

            /*
             * Lit le fichier re�u et �crit son contenu dans un fichier sur le
             * disque.
             */
            byte[] tampon = new byte[TAILLE_TAMPON];
            int longueur;
            while ( ( longueur = entree.read( tampon ) ) > 0 ) {
                sortie.write( tampon, 0, longueur );
            }
        } finally {
            try {
                sortie.close();
            } catch ( IOException ignore ) {
            }
            try {
                entree.close();
            } catch ( IOException ignore ) {
            }
        }
    }
}
