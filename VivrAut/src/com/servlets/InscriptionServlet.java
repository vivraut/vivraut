package com.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.ServiceTutorDAO;
import com.entities.Tutor;
import com.formulaires.ValidationInscription;

@SuppressWarnings("serial")
/*@WebServlet("/inscription")*/
@WebServlet( name="InscriptionServlet", urlPatterns = "/inscription" )
public class InscriptionServlet extends HttpServlet{
	public static final String ATT_TUTEUR = "tuteur";
    public static final String ATT_FORM   = "vi";

    public static final String VUE_SUCCES = "/WEB-INF/succesInscription.jsp";
    public static final String VUE_FORM   = "/WEB-INF/inscription.jsp";
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		getServletContext().getRequestDispatcher("/WEB-INF/accueil.jsp").forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ValidationInscription vi = new ValidationInscription();

        /* Traitement de la requ�te et r�cup�ration du bean en r�sultant */
        Tutor tutor = vi.creerTuteur(request);
        
        /* Ajout du bean et de l'objet m�tier � l'objet requ�te */
        request.setAttribute( ATT_TUTEUR, tutor );
        request.setAttribute( ATT_FORM, vi );

        if (vi.getErreurs().isEmpty()){
        	ServiceTutorDAO stdao = new ServiceTutorDAO();
        	stdao.ajouter(tutor);
        	getServletContext().getRequestDispatcher(VUE_SUCCES).forward(request, response);
        }
        else
        	getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
	}
}
