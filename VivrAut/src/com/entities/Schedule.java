package com.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the schedule database table.
 * 
 */
@Entity
@NamedQuery(name="Schedule.findAll", query="SELECT s FROM Schedule s")
public class Schedule implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_SCHEDULE")
	private int idSchedule;

	@Temporal(TemporalType.TIMESTAMP)
	private Date enddate;

	@Temporal(TemporalType.TIMESTAMP)
	private Date startdate;

	//bi-directional many-to-one association to Activity
	@ManyToOne
	@JoinColumn(name="ID_ACTIVITY")
	private Activity activity;

	//bi-directional many-to-one association to Autism
	@ManyToOne
	private Autism autism;

	public Schedule() {
	}

	public int getIdSchedule() {
		return this.idSchedule;
	}

	public void setIdSchedule(int idSchedule) {
		this.idSchedule = idSchedule;
	}

	public Date getEnddate() {
		return this.enddate;
	}

	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}

	public Date getStartdate() {
		return this.startdate;
	}

	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	public Activity getActivity() {
		return this.activity;
	}

	public void setActivity(Activity activity) {
		this.activity = activity;
	}

	public Autism getAutism() {
		return this.autism;
	}

	public void setAutism(Autism autism) {
		this.autism = autism;
	}

}