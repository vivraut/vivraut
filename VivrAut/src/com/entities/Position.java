package com.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the position database table.
 * 
 */
@Entity
@NamedQuery(name="Position.findAll", query="SELECT p FROM Position p")
public class Position implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_POSITION")
	private int idPosition;

	@Temporal(TemporalType.TIMESTAMP)
	private Date date;

	@Column(name="LATITUDE_POSITION")
	private BigDecimal latitudePosition;

	@Column(name="LONGITUDE_POSITION")
	private BigDecimal longitudePosition;

	//bi-directional many-to-one association to Autism
	@ManyToOne
	@JoinColumn(name="ID_AUTISM")
	private Autism autism;

	public Position() {
	}

	public int getIdPosition() {
		return this.idPosition;
	}

	public void setIdPosition(int idPosition) {
		this.idPosition = idPosition;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public BigDecimal getLatitudePosition() {
		return this.latitudePosition;
	}

	public void setLatitudePosition(BigDecimal latitudePosition) {
		this.latitudePosition = latitudePosition;
	}

	public BigDecimal getLongitudePosition() {
		return this.longitudePosition;
	}

	public void setLongitudePosition(BigDecimal longitudePosition) {
		this.longitudePosition = longitudePosition;
	}

	public Autism getAutism() {
		return this.autism;
	}

	public void setAutism(Autism autism) {
		this.autism = autism;
	}

}