/**
 * 
 */
package com.dao;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.entities.Activity;
import com.entities.Autism;

/**
 * @author Administrateur
 *
 */
public class ServiceAutismDAO extends AbstractServiceDAO<Autism> {
	public Autism List(int idTutor){
	
	EntityManager manager = ENTITY_MANAGER_FACTORY.createEntityManager();
    EntityTransaction transaction = null;

    try {
        // Get a transaction
        transaction = manager.getTransaction();
        // Begin the transaction
        transaction.begin();
        // Get the Formation object
        Query query = manager.createQuery("SELECT a FROM Autism a WHERE a.ID_TUTOR = " + idTutor);
      //  query.setParameter(1,1);
        //return (Autism) query.getSingleResult();
        return(Autism) query.getResultList();
        //return a;
    } catch (Exception ex) {
        if (transaction != null) {
            transaction.rollback();
        }
        ex.printStackTrace();
        return null;
    } finally {
        manager.close();
    }
    }
	}
