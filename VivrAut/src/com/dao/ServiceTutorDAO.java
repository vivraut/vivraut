package com.dao;

import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import com.entities.Tutor;


public class ServiceTutorDAO extends AbstractServiceDAO<Tutor>{
	
	public Tutor trouverInscription(String email, String password) {
		/*return newTransactionS((manager) -> {
			Query query = manager.createQuery("SELECT t FROM Tutor t WHERE t.mailTutor = ? AND t.passwordTutor = ?");
	        query.setParameter(1,email);
	        query.setParameter(2,password);
	        return (Tutor) query.getSingleResult();
		});*/
		
		Tutor t = null;
		HashMap<String, Object> filtre = new HashMap<>();
		filtre.put("mailTutor", email);
		filtre.put("passwordTutor", password);
		
		List<Tutor> lt = lister(filtre);
		if(!lt.isEmpty())
			t = lt.get(0);
		
		return t;
	}
        
		public void update(Tutor tutor) {
	        // Create an EntityManager
	        EntityManager manager = ENTITY_MANAGER_FACTORY.createEntityManager();
	        EntityTransaction transaction = null;

	        try {
	            // Get a transaction
	            transaction = manager.getTransaction();
	            // Begin the transaction
	            transaction.begin();
	            // Get the Formation object
	            Tutor frm = manager.find(Tutor.class, tutor.getIdTutor());

	            // Change the values
	           
	            if(tutor.getFirstnameTutor()!=null)
	            	frm.setFirstnameTutor(tutor.getFirstnameTutor());
	            if(tutor.getLastnameTutor()!=null)
	            	frm.setLastnameTutor(tutor.getLastnameTutor());
	            if(tutor.getMailTutor()!=null)
	            	frm.setMailTutor(tutor.getMailTutor());
	            if(tutor.getPasswordTutor()!=null)
	            	frm.setPasswordTutor(tutor.getPasswordTutor());
	            if(tutor.getPhoneNumberTutor()!=null)
	            	frm.setPhoneNumberTutor(tutor.getPhoneNumberTutor());

	            // Update the Formation
	            manager.persist(frm);

	            // Commit the transaction
	            transaction.commit();
	        } catch (Exception ex) {
	            // If there are any exceptions, roll back the changes
	            if (transaction != null) {
	                transaction.rollback();
	            }
	            // Print the Exception
	            ex.printStackTrace();
	        } finally {
	            // Close the EntityManager
	            manager.close();
	        }
	    }  
		/*Tutor t = null;
        // Create an EntityManager
        EntityManager manager = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction transaction = null;

        try {
            transaction = manager.getTransaction();
            transaction.begin();
            Query query = manager.createQuery("SELECT t FROM Tutor t WHERE t.mailTutor = ? AND t.passwordTutor = ?");
            query.setParameter(1,email);
            query.setParameter(2,password);
            t = (Tutor) query.getSingleResult();
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        } finally {
            manager.close();
        }
        return t;*/
	
}
