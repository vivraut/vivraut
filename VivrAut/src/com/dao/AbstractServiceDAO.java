package com.dao;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.entities.Tutor;

/**
 * Impl�mentation g�n�rique des m�thodes de l'interface ServiveDAO.
 * 
 * @see ServiceDAO
 * @param <T>
 *            Type de l'entit�
 */
public abstract class AbstractServiceDAO<T> implements ServiceDAO<T> {
	protected static final EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence
			.createEntityManagerFactory("vivrautdb");

	@SuppressWarnings("unchecked")
	protected final Class<T> ENTITY_CLASS = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass())
			.getActualTypeArguments()[0];

	private EntityManager manager;

	/**
	 * Execute une nouvelle transaction qui ne prend pas de param�tre et ne
	 * retourne pas de r�sultat.
	 * 
	 * @param action
	 *            Code � ex�cuter durant la transaction.
	 */
	protected void newTransaction(Consumer<EntityManager> action) {
		newTransactionC(action, (m, a) -> a.accept(m));
	}

	/**
	 * Execute une nouvelle transaction qui prend un param�tre en entr�e et ne
	 * retourne pas de r�sultat.
	 * 
	 * @param in
	 *            Param�tre d'entr�e.
	 * @param action
	 *            Code � ex�cuter durant la transaction.
	 */
	protected <TIn> void newTransactionC(TIn in, BiConsumer<EntityManager, TIn> action) { // Consumer
		newTransactionF(in, (m, i) -> {
			action.accept(m, i);
			return null;
		});
	}

	/**
	 * Execute une nouvelle transaction qui ne prend pas de param�tre d'entr�e
	 * et retourne un r�sultat.
	 * 
	 * @param action
	 *            Code � ex�cuter durant la transaction.
	 * @return Retour du param�tre action.
	 */
	protected <TOut> TOut newTransactionS(Function<EntityManager, TOut> action) { // Supplier
		return newTransactionF(action, (m, a) -> a.apply(m));
	}

	/**
	 * Execute une nouvelle transaction qui prend un param�tre d'entr�e et
	 * retourne un r�sultat.
	 * 
	 * @param in
	 *            Param�tre d'entr�e.
	 * @param action
	 *            Code � ex�cuter durant la transaction.
	 * @return Retour du param�tre action.
	 */
	protected <TIn, TOut> TOut newTransactionF(TIn in, BiFunction<EntityManager, TIn, TOut> action) { // Function
		TOut out = null;
		boolean opener = (manager == null || !manager.isOpen());
		if (opener) {
			manager = ENTITY_MANAGER_FACTORY.createEntityManager();
		}
		EntityTransaction transaction = null;

		try {
			transaction = manager.getTransaction();
			if (opener)
				transaction.begin();
			out = action.apply(manager, in);
			if (opener && !transaction.getRollbackOnly())
				transaction.commit();
		} catch (Exception ex) {
			ex.printStackTrace();
			if (transaction != null && transaction.isActive()) {
				transaction.rollback();
			}
		} finally {
			if (opener)
				manager.close();
		}

		return out;
	}

	/**
	 * Ajoute un �l�ment � la base de donn�es et retourne l'�l�ment actualis�.
	 * 
	 * @param entite
	 *            El�ment � ajouter.
	 * @see com.dao.ServiceDAO#ajouter(java.lang.Object)
	 */
	@Override
	public T ajouter(T entite) {
		return newTransactionS((manager) -> manager.merge(entite));
	}

	/**
	 * Supprime un �l�ment de la base de donn�e.
	 * 
	 * @see com.dao.ServiceDAO#supprimer(int)
	 * @param id
	 *            id de l'�l�ment � supprimer.
	 */
	@Override
	public void supprimer(int id) {
		newTransaction((manager) -> {
			T t = manager.find(ENTITY_CLASS, id);
			manager.remove(t);
		});
	}

	/**
	 * Liste l'ensemble des �l�ments existant dans la base de donn�es.
	 * 
	 * @see com.dao.ServiceDAO#lister()
	 */
	@Override
	public List<T> lister() {
		return newTransactionS((manager) -> {
			TypedQuery<T> query = manager.createNamedQuery(ENTITY_CLASS.getSimpleName() + ".findAll", ENTITY_CLASS);
			return query.getResultList();
		});
	}

	/**
	 * Liste l'ensemble des �l�ments existant dans la base de donn�es respectant
	 * le filtre donn�e.
	 * 
	 * @param filtre
	 *            Ensemble de cl�s/valeurs. Les cl�s repr�sentent les noms de
	 *            colonnes et les valeurs la valeur � chercher.
	 * @see com.dao.ServiceDAO#lister()
	 */
	public List<T> lister(final Map<String, Object> filtre) {
		return newTransactionS((manager) -> {
			Object[] values = filtre.values().toArray();
			Object[] keys = filtre.keySet().toArray(); // Forc�ment des String
			String subquery = "e." + keys[0] + " = ?";

			for (int i = 1; i < values.length; i++) {
				subquery = String.join(" AND ", subquery, "e." + keys[i] + " = ?");
			}
			Query query = manager.createQuery("SELECT e FROM " + ENTITY_CLASS.getSimpleName() + " e WHERE " + subquery);

			for (int i = 0; i < values.length; i++) {
				query.setParameter(i + 1, values[i]);
			}

			@SuppressWarnings("unchecked")
			List<T> result = (List<T>) query.getResultList();

			return result;
		});
	}

	/**
	 * R�cup�re un �l�ment dans la base de donn�es.
	 * 
	 * @param id
	 *            id de l'�l�ment � r�cup�rer.
	 * @see com.dao.ServiceDAO#trouver(int)
	 */
	@Override
	public T trouver(int id) {
		return newTransactionS((manager) -> manager.find(ENTITY_CLASS, id));
	}

	/**
	 * Vide l'ensemble des �l�ments de la table associ�e dans la base de
	 * donn�es.
	 * 
	 * @see com.dao.ServiceDAO#vider()
	 */
	@Override
	public void vider() {
		newTransaction((manager) -> {
			List<T> entities = lister();
			for (T e : entities)
				manager.remove(e);
		});
	}

}
