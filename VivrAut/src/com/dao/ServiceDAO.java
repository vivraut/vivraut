package com.dao;

import java.util.List;

public interface ServiceDAO<T> {

	T ajouter (T entite);
	void supprimer (int id);
	List<T> lister ();
	T trouver (int id);
	void vider();
	
}
