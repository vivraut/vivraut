package com.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import com.entities.Activity;
import com.entities.Location;
import com.entities.Tutor;

public class ServiceActivityDAO extends AbstractServiceDAO<Activity> {
	public Activity trouver2(int id){
		EntityManager manager = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction transaction = null;

        try {
            // Get a transaction
            transaction = manager.getTransaction();
            // Begin the transaction
            transaction.begin();
            // Get the Formation object
            Query query = manager.createQuery("SELECT a FROM Activity a WHERE a.idActivity = ?");
            query.setParameter(1,1);
            return (Activity) query.getSingleResult();
            //return a;
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
            return null;
        } finally {
            manager.close();
        }
        
	}
	
	public void update(Activity activite, Location location) {
        // Create an EntityManager
        EntityManager manager = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction transaction = null;

        try {
            // Get a transaction
            transaction = manager.getTransaction();
            // Begin the transaction
            transaction.begin();
            // Get the Formation object
            Activity frm = manager.find(Activity.class, activite.getIdActivity());
            //System.out.println("Id loc :" + location.getIdLocation());
            Location loc = manager.find(Location.class, location.getIdLocation());

            // Change the values
            frm.setDefaultSecondDurationActivity(activite.getDefaultSecondDurationActivity());
            frm.setUrlPictureActivity(activite.getUrlPictureActivity());
            frm.setNameActivity(activite.getNameActivity());
            if(loc!=null){
	            loc.setAddressLocation(location.getAddressLocation());
	            loc.setCityLocation(location.getCityLocation());
	            loc.setLatitudeLocation(location.getLatitudeLocation());
	            loc.setGitudeLocation(location.getGitudeLocation());
	            loc.setNameLocation(location.getNameLocation());
	            loc.setPostalCodeLocation(location.getPostalCodeLocation());
	            frm.setLocation(loc);
            }

            // Update the Formation
            manager.persist(frm);

            // Commit the transaction
            transaction.commit();
        } catch (Exception ex) {
            // If there are any exceptions, roll back the changes
            if (transaction != null) {
                transaction.rollback();
            }
            // Print the Exception
            ex.printStackTrace();
        } finally {
            // Close the EntityManager
            manager.close();
        }
    }  
	
	public void update(Activity activite) {
        // Create an EntityManager
        EntityManager manager = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction transaction = null;

        try {
            // Get a transaction
            transaction = manager.getTransaction();
            // Begin the transaction
            transaction.begin();
            // Get the Formation object
            Activity frm = manager.find(Activity.class, activite.getIdActivity());
            

            // Change the values
            frm.setDefaultSecondDurationActivity(activite.getDefaultSecondDurationActivity());
            frm.setUrlPictureActivity(activite.getUrlPictureActivity());
            frm.setNameActivity(activite.getNameActivity());
            

            // Update the Formation
            manager.persist(frm);

            // Commit the transaction
            transaction.commit();
        } catch (Exception ex) {
            // If there are any exceptions, roll back the changes
            if (transaction != null) {
                transaction.rollback();
            }
            // Print the Exception
            ex.printStackTrace();
        } finally {
            // Close the EntityManager
            manager.close();
        }
    }  
}
