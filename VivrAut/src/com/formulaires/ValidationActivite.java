package com.formulaires;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import com.entities.Activity;
import com.entities.Autism;
import com.entities.Location;

public class ValidationActivite {
	private static final String CHAMP_NOM = "nom";
	private static final String CHAMP_URL = "url";
	private static final String CHAMP_DUREE = "duree";
	private static final String CHAMP_NOMLIEU = "nomLieu";
	private static final String CHAMP_ADRESSE = "adresse";
	private static final String CHAMP_CODEPOSTAL = "codePostal";
	private static final String CHAMP_VILLE = "ville";
	private static final String CHAMP_PAYS = "pays";
	private static final String CHAMP_LONGITUDE = "longitude";
	private static final String CHAMP_LATITUDE = "latitude";
	
	private String resultat;
	private Map<String, String> erreurs = new HashMap<String, String>();
	
	public Map<String, String> getErreurs() {
		return erreurs;
	}

	public String getResultat() {
		return resultat;
	}
	
	public Activity creerActivite(HttpServletRequest request, Autism aut, Location location, Activity activite) {
		String nom = getValeurChamp(request, CHAMP_NOM);
		String url = getValeurChamp(request, CHAMP_URL);
		String duree = getValeurChamp(request, CHAMP_DUREE);
		
		if (activite==null)
			activite = new Activity();
		
		try {
			validationNom(nom);
		} catch (Exception e) {
			setErreur(CHAMP_NOM, e.getMessage());
		}
		activite.setNameActivity(nom);
		
		activite.setUrlPictureActivity(url);
		
		try {
			validationDuree(duree);
		} catch (Exception e) {
			setErreur(CHAMP_DUREE, e.getMessage());
		}
		if(duree!=""&&duree!=null)
			activite.setDefaultSecondDurationActivity(Integer.parseInt(duree));
		else
			activite.setDefaultSecondDurationActivity(null);
		activite.setLocation(location);
		activite.setAutism(aut);
		return activite;
	}
	
	public Location creerLocation(HttpServletRequest request, Location location) {
		String nom = getValeurChamp(request, CHAMP_NOM); //utile si nomLieu non indiqu�
		String nomLieu = getValeurChamp(request, CHAMP_NOMLIEU);
		String adresse = getValeurChamp(request, CHAMP_ADRESSE);
		String codePostal = getValeurChamp(request, CHAMP_CODEPOSTAL);
		String ville = getValeurChamp(request, CHAMP_VILLE);
		String pays = getValeurChamp(request, CHAMP_PAYS);
		String longitude = getValeurChamp(request, CHAMP_LONGITUDE);
		String latitude = getValeurChamp(request, CHAMP_LATITUDE);
		
		if (location==null)
			location = new Location();
		if((nomLieu==""||nomLieu==null)&&(adresse==""||adresse==null)&&(codePostal==""||codePostal==null)&&(ville==""||ville==null)
			&&(pays==""||pays==null)&&(longitude==""||longitude==null)&&(latitude==""||latitude==null))
			return null;
		
		try {
			validationNomLieu(nomLieu);
		} catch (Exception e) {
			setErreur(CHAMP_NOMLIEU, e.getMessage());
		}
		if(nomLieu==""||nomLieu==null)
			location.setNameLocation(nom);
		else location.setNameLocation(nomLieu);
		
		try {
			validationAdresse(adresse);
		} catch (Exception e) {
			setErreur(CHAMP_ADRESSE, e.getMessage());
		}
		location.setAddressLocation(adresse);
		
		try {
			validationCodePostal(codePostal);
		} catch (Exception e) {
			setErreur(CHAMP_CODEPOSTAL, e.getMessage());
		}
		location.setPostalCodeLocation(codePostal);
		
		try {
			validationVille(ville);
		} catch (Exception e) {
			setErreur(CHAMP_VILLE, e.getMessage());
		}
		location.setCityLocation(ville);
		
		try {
			validationPays(pays);
		} catch (Exception e) {
			setErreur(CHAMP_PAYS, e.getMessage());
		}
		
		
		try {
			validationLongitude(longitude);
		} catch (Exception e) {
			setErreur(CHAMP_LONGITUDE, e.getMessage());
		}
		if(longitude!=""&&longitude!=null)
			location.setGitudeLocation(BigDecimal.valueOf(Double.parseDouble(longitude)));
		else
			location.setGitudeLocation(new BigDecimal(0));
		try {
			validationLatitude(latitude);
		} catch (Exception e) {
			setErreur(CHAMP_LATITUDE, e.getMessage());
		}
		if(latitude!=""&&latitude!=null)
			location.setLatitudeLocation(BigDecimal.valueOf(Double.parseDouble(latitude)));
		else
			location.setLatitudeLocation(new BigDecimal(0));
		return location;
		
	}
	
	public Activity creerActivite(HttpServletRequest request, Autism aut, Location location) {
		String nom = getValeurChamp(request, CHAMP_NOM);
		String url = getValeurChamp(request, CHAMP_URL);
		String duree = getValeurChamp(request, CHAMP_DUREE);
		
		Activity activite = new Activity();
		
		try {
			validationNom(nom);
		} catch (Exception e) {
			setErreur(CHAMP_NOM, e.getMessage());
		}
		activite.setNameActivity(nom);

		activite.setUrlPictureActivity(url);
		
		try {
			validationDuree(duree);
		} catch (Exception e) {
			setErreur(CHAMP_DUREE, e.getMessage());
		}
		if(duree!=""&&duree!=null)
			activite.setDefaultSecondDurationActivity(Integer.parseInt(duree));
		else
			activite.setDefaultSecondDurationActivity(null);
		activite.setLocation(location);
		activite.setAutism(aut);
		return activite;
	}
	
	public Location creerLocation(HttpServletRequest request) {
		String nom = getValeurChamp(request, CHAMP_NOM); //utile si nomLieu non indiqu�
		String nomLieu = getValeurChamp(request, CHAMP_NOMLIEU);
		String adresse = getValeurChamp(request, CHAMP_ADRESSE);
		String codePostal = getValeurChamp(request, CHAMP_CODEPOSTAL);
		String ville = getValeurChamp(request, CHAMP_VILLE);
		String pays = getValeurChamp(request, CHAMP_PAYS);
		String longitude = getValeurChamp(request, CHAMP_LONGITUDE);
		String latitude = getValeurChamp(request, CHAMP_LATITUDE);
		
		Location location = new Location();
		if((nomLieu==""||nomLieu==null)&&(adresse==""||adresse==null)&&(codePostal==""||codePostal==null)&&(ville==""||ville==null)
			&&(pays==""||pays==null)&&(longitude==""||longitude==null)&&(latitude==""||latitude==null))
			return null;
		
		try {
			validationNomLieu(nomLieu);
		} catch (Exception e) {
			setErreur(CHAMP_NOMLIEU, e.getMessage());
		}
		if(nomLieu==""||nomLieu==null)
			location.setNameLocation(nom);
		else location.setNameLocation(nomLieu);
		
		try {
			validationAdresse(adresse);
		} catch (Exception e) {
			setErreur(CHAMP_ADRESSE, e.getMessage());
		}
		location.setAddressLocation(adresse);
		
		try {
			validationCodePostal(codePostal);
		} catch (Exception e) {
			setErreur(CHAMP_CODEPOSTAL, e.getMessage());
		}
		location.setPostalCodeLocation(codePostal);
		
		try {
			validationVille(ville);
		} catch (Exception e) {
			setErreur(CHAMP_VILLE, e.getMessage());
		}
		location.setCityLocation(ville);
		
		try {
			validationPays(pays);
		} catch (Exception e) {
			setErreur(CHAMP_PAYS, e.getMessage());
		}

		
		try {
			validationLongitude(longitude);
		} catch (Exception e) {
			setErreur(CHAMP_LONGITUDE, e.getMessage());
		}
		if(longitude!=""&&longitude!=null)
			location.setGitudeLocation(BigDecimal.valueOf(Double.parseDouble(longitude)));
		else
			location.setGitudeLocation(new BigDecimal(0));
		try {
			validationLatitude(latitude);
		} catch (Exception e) {
			setErreur(CHAMP_LATITUDE, e.getMessage());
		}
		if(latitude!=""&&latitude!=null)
			location.setLatitudeLocation(BigDecimal.valueOf(Double.parseDouble(latitude)));
		else
			location.setLatitudeLocation(new BigDecimal(0));
		return location;
		
	}
	
	private void validationNom(String nom) throws Exception {
		if (nom != null){
			if (nom.length() < 2)
				throw new Exception("Le nom de l'activit� doit contenir au moins 2 caract�res.");
		} else
			throw new Exception("Vous devez saisir une nom d'activit� d'au moins 2 caract�res.");
	}
	
	private void validationDuree(String duree) throws Exception {
		if (duree != null && !duree.matches("^[0-9]{1,10}")) 
			throw new Exception("La dur�e doit �tre un nombre.");
	}
	
	private void validationNomLieu(String nomLieu) throws Exception {
		if (nomLieu != null && nomLieu.length()<2)
			throw new Exception("Le nom du lieu doit contenir au moins 2 caract�res.");
	}
	
	private void validationAdresse(String adresse) throws Exception {
		if (adresse != null && adresse.length() < 2)
			throw new Exception("L'adresse doit contenir au moins 2 caract�res.");
	}
	
	private void validationCodePostal(String codePostal) throws Exception {
		if (codePostal != null && codePostal.length()<2)
			throw new Exception("Le code postal doit contenir au moins 2 caract�res.");
	}
	
	private void validationVille(String ville) throws Exception {
		if (ville != null && ville.length()<2)
			throw new Exception("Le nom de la ville doit contenir au moins 2 caract�res.");
	}
	
	private void validationPays(String pays) throws Exception {
		if (pays != null && pays.length()<2)
			throw new Exception("Le nom du pays doit contenir au moins 2 caract�res.");
	}
	
	private void validationLongitude(String longitude) throws Exception {
		if (longitude != null && !longitude.matches("^-?[0-9]+(.[0-9]+)?$")) 
			throw new Exception("La longitude doit �tre un nombre.");
	}
	
	private void validationLatitude(String latitude) throws Exception {
		if (latitude != null && !latitude.matches("^-?[0-9]+(.[0-9]+)?$")) 
			throw new Exception("La latitude doit �tre un nombre.");
	}
	
	private void setErreur(String champ, String message) {
		erreurs.put(champ, message);
	}

	private static String getValeurChamp(HttpServletRequest request, String nomChamp) {
		String valeur = request.getParameter(nomChamp);
		if (valeur == "" || valeur == null || valeur.trim().length() == 0) {
			return null;
		} else {
			return valeur;
		}
	}
}
