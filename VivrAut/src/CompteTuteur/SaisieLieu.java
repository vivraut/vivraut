package CompteTuteur;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import com.dao.ServiceLocationDAO;
import com.dao.ServiceTutorDAO;
import com.entities.Autism;
import com.entities.Location;
import com.entities.Tutor;

import java.util.HashMap;
import java.util.Map;

/**
 * Servlet implementation class SaisieLieu SI CA MARCHE JE TUE LE CHIEN EN COURS DE DEV
 */

/*@WebServlet("/SaisieLieu")*/
@WebServlet( name="SaisieLieu", urlPatterns = "/saisieLieu" )

public class SaisieLieu extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public static final String VUE = "/WEB-INF/saisieLieu.jsp";
	
	public  int ID_LOCATION;
	public  int tutor_ID_TUTOR;
    public static final String ATT_ERREURS  = "erreurs";
    public static final String ATT_RESULTAT = "resultat";
    public static final String CHAMP_TUTOR   = "tutor";
    
    String NAME_LOCATION=null;
    String ADDRESS_LOCATION=null; 
    String POSTAL_CODE_LOCATION=null; 
    String CITY_LOCATION=null; 
    String GITUDE_LOCATION=null; 
    String LATITUDE_LOCATION=null; 
    
        
    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
        /* Affichage de la page de cr�ation du lieu */
        this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
    }
    
    public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
        String resultat;
        Map<String, String> erreurs = new HashMap<String, String>();
    	
    	/* R�cup�ration des champs du formulaire. */
        NAME_LOCATION = request.getParameter("f_NAME_LOCATION");
        ADDRESS_LOCATION = request.getParameter("f_ADDRESS_LOCATION");
        POSTAL_CODE_LOCATION = request.getParameter( "f_POSTAL_CODE_LOCATION");
        CITY_LOCATION = request.getParameter( "f_CITY_LOCATION");
        GITUDE_LOCATION = request.getParameter( "f_GITUDE_LOCATION");
        LATITUDE_LOCATION = request.getParameter( "f_LATITUDE_LOCATION");        
       
        
        /* Validation du champ nom. */
        try {
            validationNom(NAME_LOCATION,request);
        } catch ( Exception e ) {
            erreurs.put( "NAME_LOCATION", e.getMessage() );
        }
    
    /* Initialisation du r�sultat global de la validation. */
    if (erreurs.isEmpty() ) {
    	//ici procedure envoi dans la base
    	enregistrerLieu(request);
        resultat = "Succ�s de l'inscription.";
    } else {
        resultat = "�chec de l'inscription.";
    }

    /* Stockage du r�sultat et des messages d'erreur dans l'objet request */
    request.setAttribute( ATT_ERREURS, erreurs );
    request.setAttribute( ATT_RESULTAT, resultat );

    /* Transmission de la paire d'objets request/response � notre JSP */
    this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
    }
  
    
    /**
     * Valide le nom du lieu saisi.
     */
    private void validationNom(String NAME_LOCATION , HttpServletRequest request) throws Exception {
        if ( NAME_LOCATION == null) {
            System.out.println("La saisie du nom de lieu est obligatoire" + NAME_LOCATION);
            enregistrerLieu(request);
            throw new Exception( "La saisie du nom de lieu est obligatoire." );
        }
    }

    

        
        private void enregistrerLieu(HttpServletRequest request){
        	// On instancie les classes service DAO LE DAO EST IL PRET ?????
        	com.dao.ServiceTutorDAO stdao = new ServiceTutorDAO();
        	com.dao.ServiceLocationDAO sldao = new ServiceLocationDAO();
        	
        	// On peut utiliser les m�thodes des classes service instanci�es
        	HttpSession session = request.getSession();
			Tutor tu = (Tutor)session.getAttribute(CHAMP_TUTOR);
        	Location Lieu=new Location(
        			NAME_LOCATION,
        			ADDRESS_LOCATION, 
        			POSTAL_CODE_LOCATION, 
        			CITY_LOCATION, 
        			BigDecimal.valueOf(Double.parseDouble(GITUDE_LOCATION)), 
        			BigDecimal.valueOf(Double.parseDouble(LATITUDE_LOCATION)), 
        			tu);
        	sldao.ajouter(Lieu);
			
        }
    }